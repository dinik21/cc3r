﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ARCA;

namespace CC3R
{
    public partial class frmCTCoin : Form
    {
        public RS232 mySerial;
        public STRUCTURES myStruct;
        public FUNCTIONS myFunc;
        public STRUCTURES.SCCTalkAnswer ccTalkAnswer = new STRUCTURES.SCCTalkAnswer();
        public STRUCTURES.SCtCoinProtocol myCommand;
        public STRUCTURES.SCtCoinProtocol myAnswer;
        public string command;
        public frmCTCoin(string comPort)
        {
            InitializeComponent();
            InitializeComunication(comPort);
        }
        void InitializeComunication(string comPort)
        {
            mySerial = new RS232();
            myFunc = new FUNCTIONS();
            mySerial.ConfigCom(comPort);
            mySerial.DataSent += new EventHandler(ComDataSent);
            mySerial.DataReceived += new EventHandler(ComDataReceived);
            myCommand = new STRUCTURES.SCtCoinProtocol();
            myAnswer = new STRUCTURES.SCtCoinProtocol();
            myCommand.stx = 0x02;
            myCommand.etx = 0x03;
        }
        public bool ControlInvokeRequired(Control c, Action a)
        {
            if (c.InvokeRequired) c.Invoke(new MethodInvoker(delegate { a(); }));
            else return false;
            return true;
        }

        public void ComDataSent(object sender, EventArgs e)//
        {
            //if (ControlInvokeRequired(txtSend, () => ComDataSent(sender, null))) return;
            //txtSend.Text = sender.ToString();
        }

        public void ComDataReceived(object sender, EventArgs e)
        {
            //if (ControlInvokeRequired(txtHex, () => ComDataReceived(sender, null))) return;
            //txtHex.Text = mySerial.commandAnswer;
            //txtAscii.Text = myFunc.Hex2Ascii(mySerial.commandAnswer);
            myAnswer = new STRUCTURES.SCtCoinProtocol();
            if (mySerial.commandAnswer.Length>0)
            {
                myAnswer.error = Convert.ToByte(mySerial.commandAnswer.Substring(0, 2));
                if (myAnswer.error==0x06)
                {
                    myAnswer.stx = Convert.ToByte(mySerial.commandAnswer.Substring(2, 2));
                    myAnswer.nData = Convert.ToByte(myFunc.Hex2Dec(mySerial.commandAnswer.Substring(4, 2)));
                    myAnswer.data = new byte[myAnswer.nData];
                    for (int i = 0; i < myAnswer.nData; i++)
                    {
                        myAnswer.data[i] = Convert.ToByte(myFunc.Hex2Dec(mySerial.commandAnswer.Substring(i + 6, 2)));
                        myAnswer.answer += mySerial.commandAnswer.Substring(i * 2 + 6, 2); 
                    }
                    //myAnswer.nData = Convert.ToByte(myFunc.Hex2Dec(mySerial.commandAnswer.Substring(4, 2))); //Convert.ToByte(mySerial.commandAnswer.Length-2);  //Convert.ToByte(myFunc.Hex2Dec(mySerial.commandAnswer.Substring(4, 2)));
                    //myAnswer.data = new byte[(mySerial.commandAnswer.Length - 12) / 2];//Convert.ToInt16(myAnswer.nData)
                    //for (int i = 0; i < myAnswer.data.Length; i++)
                    //{
                    //    myAnswer.data[i] = Convert.ToByte(myFunc.Hex2Dec(mySerial.commandAnswer.Substring(i + 6, 2)));
                    //    myAnswer.answer += mySerial.commandAnswer.Substring(i * 2 + 6, 2); //(i*2 + 6, 2)
                    //}
                    myAnswer.crcHi = Convert.ToByte(myFunc.Hex2Dec(mySerial.commandAnswer.Substring(mySerial.commandAnswer.Length -  6, 2)));
                    myAnswer.crcLo = Convert.ToByte(myFunc.Hex2Dec(mySerial.commandAnswer.Substring(mySerial.commandAnswer.Length - 4, 2)));
                    myAnswer.etx = Convert.ToByte(myFunc.Hex2Dec(mySerial.commandAnswer.Substring(mySerial.commandAnswer.Length - 2, 2)));
                }
            }
        }
        private void frmCTCoin_Load(object sender, EventArgs e)
        {

        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            mySerial.OpenCom();
            mySerial.SendCommand(myFunc.CtCoinConnect());
            mySerial.CloseCom();
            lblConnection.Text = myAnswer.answer;
        }

        private void btnDisconnect_Click(object sender, EventArgs e)
        {
            mySerial.SendCommand(myFunc.CtCoinDisconnect());
            mySerial.CloseCom();
            lblConnection.Text = myAnswer.answer;
        }

        private void btnDisplayCont_Click(object sender, EventArgs e)
        {
            mySerial.SendCommand(myFunc.CtCoinGetDisplayContent());
            mySerial.CloseCom();
            txtDisplay.Text=myFunc.Hex2Ascii(myAnswer.answer.Substring(4,40));
            txtDisplay.Text += Environment.NewLine;
            txtDisplay.Text += myFunc.Hex2Ascii(myAnswer.answer.Substring(44));
        }

        private void btnSingleCommand_Click(object sender, EventArgs e)
        {
            txtSingleAnswer.Text = "";
            mySerial.SendCommand(myFunc.CtCoinCommandConstructor(txtSingleCommand.Text));
            mySerial.CloseCom();
            txtSingleAnswer.Text = myAnswer.answer;
            btnDisplayCont_Click(this,null);
        }

        private void btnGetKeypadPress_Click(object sender, EventArgs e)
        {
            mySerial.OpenCom();
            DateTime myTime = DateTime.Now;
            string key = "";
            mySerial.SendCommand(myFunc.CtCoinSetKeyboardInputMode());
            while (DateTime.Now.Subtract(myTime).TotalSeconds < 30)
            {
                mySerial.SendCommand(myFunc.CtCoinGetKeyboardBuffer());
                if (mySerial.commandAnswer.Substring(14,2) != "00")
                {
                    
                    for (int i = 14; i <= 52; i+=2)
                    {
                        key += myFunc.Hex2Ascii(mySerial.commandAnswer.Substring(i, 2));
                    }
                    break;
                }
            }
            mySerial.CloseCom();
            this.Text = key;
        }
    }
}
