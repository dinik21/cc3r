﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Drawing;
using System.Runtime.InteropServices;
using System.IO;
using System.Collections;
using System.Reflection;
using System.Diagnostics;
using System.Windows.Forms;

namespace ARCA
{
    public class NoAnswerException: Exception
    {
        public NoAnswerException()
        { }
        public NoAnswerException(string message)
            :base (message)
        {

        }
    }
    public class Constants
    {
        public const string printerStatus = "1B76";
        public const string printerIdendity = "1B49";
        public const string printerLineFeed = "0A";
        public const string printerCancelBuffer = "18";
        public const string printerPartialCut = "1B6D";
        public const string printerFullCut = "1B69";
        public const string printerFeedPaper = "1B4A";
        public const string printerSelectFont = "1B25";
    }
    public class CC3R
    {
        public STRUCTURES myStruct = new STRUCTURES();
        

        public CC3R()
        {
            InitializeComponents();
        }

        void InitializeComponents()
        {
            
        }

    }
    public class CM18
    {
        public CM18()
        {
        }
    }
    public class STRUCTURES
    {
        public SColor color = new SColor();
        public SError error = new SError();
        public STRUCTURES()
        {
            InitializeComponents();
        }

        void InitializeComponents()
        {
            color.message = Color.FromArgb(255, 0, 0, 0);
            color.error = Color.FromArgb(255, 255, 0, 0);
            color.warning = Color.FromArgb(255, 255, 184, 0);
            color.action = Color.FromArgb(255, 0, 0, 255);
            color.correct = Color.FromArgb(255, 154, 205, 50);
            color.waiting = Color.FromArgb(255, 154, 205, 50);
            color.arcaBlack = Color.FromArgb(255, 88, 89, 91);
            color.arcaPurple = Color.FromArgb(255, 85, 22, 110);
            color.arcaGreen = Color.FromArgb(255, 174, 191, 55);
            color.arcaOrange = Color.FromArgb(255, 228, 86, 32);
            color.arcaBlu = Color.FromArgb(255, 26, 50, 129);
        }

        public struct SConnectionTest
        {
            public bool CtCoin;
            public bool CoinHopperDx;
            public bool CoinHopperSx;
        }

        public struct SError
        {
            public string description;
            public int id;
            public string keyPressed;
        }
        public struct STest
        {
            public string startDate;
            public string startTime;
            public string endDate;
            public string endTime;
            public string testResult;
            public SSingleTest[] test;
        }
        public struct SSingleTest
        {
            public string description;
            public string result;
            public string logID;
        }
        public struct SCtCoinCRC
        {
            public string crcHi;
            public string crcLo;
        }
        public struct SCtCoinProtocol
        {
            public byte error;
            public byte stx;
            public byte nData;
            public byte[] data;
            public byte crcHi;
            public byte crcLo;
            public byte etx;
            public string answer;
        }
        public struct SDConAnswer
        {
            public string lead;
            public string moduleAddress;
            public string data;
            public string chk;
            public string cr;
        }
        public struct SCCTalkAnswer
        {
            public string destinationAddress;
            public int bytes;
            public string sourceAddress;
            public string header;
            public string checksum;
            public string data;
            public string answer;
        }
        public struct SColor
        {
            public Color message;
            public Color error;
            public Color warning;
            public Color action;
            public Color correct;
            public Color waiting;
            public Color arcaBlack;
            public Color arcaPurple;
            public Color arcaGreen;
            public Color arcaOrange;
            public Color arcaBlu;
        }
        public struct SCC3RProduct
        {
            public SCC3RCtCoin ctCoin;
            public SCC3RKeyPad keyPad;
            public SCC3RPrinter printer;
            public SCC3RRemoteIO remoteIO;
            public SCC3RCoinHoppers coinHoppers;
            public string productCode;
            public string productSerialNumber;
            public SClient client;
            public string MACAddress;
            public string description;
            public string winSerialLabel;
            public string winKey;
        }
        public struct SCC3RPrinter
        {
            public string description;
        }
        public struct SCC3RHopper
        {
            public int address;
            public string equipCategory;
            public string manufacturerId;
            public string denomination;
            public string productCode;
            public string serialNumber;
            public SFirmware firmware;
            public int[] diameter;
            public string diameters;
            public string status;
            public string paidCoin;
        }
        public struct SCC3RCoinHoppers
        {
            public SCC3RHopper[] hoppersDx;
            public SCC3RHopper[] hoppersSx;
            //public List<SCC3RHopper> hoppers;
        }

        public struct SCC3RCtCoinDisplay
        {
            public string upLeft;
            public string upRight;
            public string downLeft;
            public string downRight;
            public string menu;
        }
        public struct SCC3RCtCoin
        {
            public int life;
            public List<SCtCoinCoin> coin;
            public SCC3RCtCoinDisplay lcd;
        }
        public struct SCtCoinCoin
        {
            public string bank;
            public string value;
            public int life;
        }
        public struct SCC3RKeyPad
        {
            public string fw;
        }
        public struct SCC3RRemoteIOPort
        {
            public string description;
            public string value;
            public string status;
        }
        public struct SCC3RRemoteIO
        {
            public string module;
            public string address;
            public SFirmware firmware;
            public string commProt;
            public SCC3RRemoteIOPort[] port;
        }
        public struct SFirmware
        {
            public string code;
            public string version;
            public string release;
            public string name;
            public string id;
            public string partNumber;
        }
        public struct SClient
        {
            public string name;
            public string ID;
        }
        public struct SCMProduct
        {
            public string productCode;
            public string productSerialNumber;
            public SClient client;
            public string family;
            public string description;
            public SFirmware suite;
            public int crm;
            public int cassetteQty;
            public int installedNumber;
            public int cd80qty;
            public string protInterface;
            public SCMCassette[] cassette;
            public SCMCassette bag;
            public SCMCassette[] cd80;
            public SRsReader reader;
            public SCMRealTimePhoto phRealTime;
            public SCMErrorLog[] errorLog;
            public SCmFwVersion version;
            public SUnitConfig cmConfig;
            public SUnitConfig cmOption;
            public SUnitConfig cmOption1;
            public SUnitConfig cmOption2;
        }
        public struct SBitUnitConfig
        {
            public string address;
            public string description;
            public bool value;
        }
        public struct SUnitConfig
        {
            public SBitUnitConfig[] bit;
        }
        public struct SCmFwVersion
        {
            public SFirmware compatibility;
            public SFirmware oscBoot;
            public SFirmware oscWce;
            public SFirmware oscxDll;
            public SFirmware oscxApp;
            public SFirmware fpga;
            public SFirmware Controller;
            public SFirmware Controller2;
            public SFirmware readerHost;
            public SFirmware readerDsp;
            public SFirmware readerFpga;
            public SFirmware readerTape;
            public SFirmware readerMagnetic;
            public SFirmware safe;
            public SFirmware cassetteA;
            public SFirmware cassetteB;
            public SFirmware cassetteC;
            public SFirmware cassetteD;
            public SFirmware cassetteE;
            public SFirmware cassetteF;
            public SFirmware cassetteG;
            public SFirmware cassetteH;
            public SFirmware cassetteI;
            public SFirmware cassetteJ;
            public SFirmware cassetteK;
            public SFirmware cassetteL;
            public SFirmware bagContrA;
            public SFirmware bagSensorA;
        }
        public struct SCMSafe
        {
            public SFirmware dirmware;
            public string serialNumber;
            public string snBoard;
            public string state;
            public string doorSim;
            public string swDoorMec;
            public string swDoorEl;
            public string mechanicalLevel;
            public string electronicLevel;
            public int motorCurrent;
            public SElm sorter;
            public SPhoto phInA;
            public SPhoto phInB;
            public SPhoto phTr2;
            public SPhoto phTr3;
        }
        public struct SCMErrorLog
        {
            public Int32 recordNumber;
            public Int32 life;
            public string opSide;
            public string opId;
            public string replyCode;
            public string F1Status;
            public string F2Status;
            public string F3Status;
            public string F4Status;
            public string FAStatus;
            public string FBStatus;
            public string FCStatus;
            public string FDStatus;
            public string FEStatus;
            public string FFStatus;
            public string FGStatus;
            public string FHStatus;
            public string FIStatus;
            public string FJStatus;
            public string FKStatus;
            public string FLStatus;
            public string FMStatus;
            public string FNStatus;
            public string FOStatus;
            public string FPStatus;
            public string line;
        }
        public struct SCMRealTimePhoto
        {
            public SPhoto inCenter;
            public SPhoto inLeft;
            public SPhoto hMax;
            public SPhoto feed;
            public SPhoto c1;
            public SPhoto shift;
            public SPhoto inq;
            public SPhoto count;
            public SPhoto Out;
            public SPhoto c3;
            public SPhoto c4a;
            public SPhoto c4b;
            public SPhoto rej;
            public SPhoto inBox;
            public SPhoto res1;
            public SPhoto res2;
            public SPhoto res3;
            public SPhoto res4;
            public SPhoto res5;
            public SPhoto res6;
            public SPhoto res7;
            public SPhoto cashLeft;
            public SPhoto cashRight;
            public SPhoto thick;
            public SPhoto finrcyc;
            public SPhoto res10;
            public SPhoto res11;
            public SPhoto res12;
            public SPhoto res13;
            public SPhoto res14;
            public SPhoto res15;
            public SPhoto res16;
            public SPhoto res17;
            public SPhoto res18;
            public SPhoto res19;
            public SPhoto res20;
            public SPhoto res21;
            public SPhoto res22;
            public SPhoto res23;
            public SPhoto res24;
            public SPhoto res25;
        }
        public struct SElm
        {
            public string name;
            public string idPar;
            public int openingTime;
            public int closingTime;
            public int currentValue;
        }
        public struct SPhoto
        {
            public string name;
            public string idPar;
            public int value;
            public int adjMin;
            public int adjMax;
            public string adjResult;
        }
        public struct SRsReader
        {
            public string serial;
            public string family;
            public string fwCode;
            public SCDF cdf;
            public string currencyMode;
            public SBankCfg[] bankCfg;
            public SCurrency[] currency;
        }
        public struct SCurrency
        {
            public string name;
            public string enabled;
            public string bankId;
            public string version;
        }
        public struct SBankCfg
        {
            public string name;
            public string enabled;
            public string bankId;
        }
        public struct SCDF
        {
            public string code;
            public string name;
        }
        public struct SCMCassette
        {
            public string type;

            public string denomination;
            public Int32 noteNumber;
            public Int32 freeCapacity;
            public string name;
            public string enabled;
            public string serial;
            public SFirmware fw;
        }
        public struct SCMVersion
        {
            public SFirmware compatibility;
            public SFirmware oscBoot;
            public SFirmware oscWce;
            public SFirmware oscxDll;
            public SFirmware oscxApp;
            public SFirmware fpga;
            public SFirmware Controller;
            public SFirmware Controller2;
            public SFirmware readerHost;
            public SFirmware readerDsp;
            public SFirmware readerFpga;
            public SFirmware readerTape;
            public SFirmware readerMagnetic;
            public SFirmware safe;
            public SFirmware cassetteA;
            public SFirmware cassetteB;
            public SFirmware cassetteC;
            public SFirmware cassetteD;
            public SFirmware cassetteE;
            public SFirmware cassetteF;
            public SFirmware cassetteG;
            public SFirmware cassetteH;
            public SFirmware cassetteI;
            public SFirmware cassetteJ;
            public SFirmware cassetteK;
            public SFirmware cassetteL;
            public SFirmware bagContrA;
            public SFirmware bagSensorA;
        }
        public struct SHCLow
        {
            public string moduleInError;
            public string moduleError;
            public string towerInTest;
            public SPhoto phInA;
            public SPhoto phInB;
            public SHcTower[] tower;
        }
        public struct SHcTower
        {
            public string status;
            public int idTower;
            public string fwRel;
            public string fwVer;
            public string name;
            public string serialNumber;
            public string snBoard;
            public string mechanicalLevel;
            public string electronicLevel;
            public string technicalLevel;
            public int motorCurrent;
            public string type;
            public SElm sorter;
            public SHcDrum[] drum;
            public SPhoto phTr1;
        }
        public struct SHcDrum
        {
            public string status;
            public string name;
            public string address;
            public string denomination;
            public int bnHeight;
            public int bnNumber;
            public SFirmware firmware;
            public string serialNumber;
            public string snHigh;
            public string snMid;
            public string snLow;
            public string snBoard;
            public string mechanicalLevel;
            public string electronicLevel;
            public string technicalLevel;
            public int motorCurrent;
            public int windTapeLenght;
            public int unwindTapeLenght;
            public int tapeLenghtDiff;
            public SElm pinchRoller;
            public SElm sorter;
            public SPhoto phIn;
            public SPhoto phEmpty;
            public SPhoto phFull;
            public SPhoto phFill;
        }
        public struct SCMConnectionParameter
        {
            public string connection;
            public string simplified;
            public string Name;
            public int baudrate;
            public StopBits stopBit;
            public Parity parity;
            public int dataBits;
            public string protocol;
            public string ipAddress;
            public string hostName;
            public int port;
        }

    }
    public class FUNCTIONS
    {
        public int keyCodePressed = 0;
        public INIFILE iniMessage;
        public STRUCTURES myStruct = new STRUCTURES();
        public Form inputBox;
        public Form messageBox;
        public Form frmLogIn;
        string inpuBoxPressed;
        public string userName;
        public string userPassword;
        public FUNCTIONS()
        {
            InitializeComponents();

        }
        void InitializeComponents()
        {
            
        }

        public string ErrorManager()
        {
            return "";
        }
        void CreateLogInForm(string user)
        {
            frmLogIn = new Form();
            TextBox txtUser = new TextBox();
            MaskedTextBox txtPsw = new MaskedTextBox();
            Label lblMessage = new Label();
            Label lblUser = new Label();
            Label lblPsw = new Label();
            Button btnOk = new Button();
            Button btnCancel = new Button();
            frmLogIn.BackColor = Color.FromArgb(85, 22, 110);
            frmLogIn.Size = new Size(380, 250);
            frmLogIn.Text = "ARCA LOGIN";
            lblMessage.ForeColor = Color.White;
            lblMessage.Font =new  Font("Microsoft Sans Serif",14,FontStyle.Regular);
            lblMessage.AutoSize = true;
            lblUser.ForeColor = Color.White;
            lblUser.Font = new Font("Microsoft Sans Serif", 14, FontStyle.Regular);
            lblUser.AutoSize = true;
            lblPsw.ForeColor = Color.White;
            lblPsw.Font = new Font("Microsoft Sans Serif", 14, FontStyle.Regular);
            lblPsw.AutoSize = true;
            lblMessage.Text = "USER LOGIN";
            lblMessage.Left = (frmLogIn.Width - lblMessage.Width)/ 2;
            lblMessage.Top = 10;
            txtUser.Font = new Font("Microsoft Sans Serif", 14, FontStyle.Regular);
            txtPsw.Font = new Font("Microsoft Sans Serif", 14, FontStyle.Regular);
            txtUser.Width = 120;
            txtPsw.Width = 120;
            txtUser.Left= (frmLogIn.Width - txtUser.Width) / 2;
            txtPsw.Left = (frmLogIn.Width - txtPsw.Width) / 2;
            txtUser.Top = 50;
            txtPsw.Top = 90;
            txtPsw.PasswordChar = '*';
            txtUser.Text = user;
            lblUser.Top = 50;
            lblPsw.Top = 90;
            lblUser.Text = "USER:";
            lblPsw.Text = "PSW:";
            lblUser.Left = ((frmLogIn.Width - txtUser.Width) / 2) - lblUser.Width;
            lblPsw.Left = ((frmLogIn.Width - txtPsw.Width) / 2) - lblPsw.Width;
            btnOk.Size = new Size(75, 45);
            btnCancel.Size = new Size(75, 45);
            btnOk.Top = 135;
            btnCancel.Top = 135;
            btnOk.Left = (frmLogIn.Width - btnOk.Width - btnCancel.Width) / 3;
            btnCancel.Left=btnOk.Width + ((frmLogIn.Width - btnOk.Width - btnCancel.Width) / 3)*2;
            btnOk.ForeColor = Color.White;
            btnCancel.ForeColor = Color.White;
            btnOk.Text = "LOGIN";
            btnCancel.Text = "CANCEL";
            btnOk.Click += (sender,e) => btnOkLoginClick(sender,e,txtUser.Text,txtPsw.Text);
            btnCancel.Click += (sender, e) => btnCancelLoginClick(sender, e);
            frmLogIn.Controls.Add(lblMessage);
            frmLogIn.Controls.Add(lblUser);
            frmLogIn.Controls.Add(lblPsw);
            frmLogIn.Controls.Add(txtUser);
            frmLogIn.Controls.Add(txtPsw);
            frmLogIn.Controls.Add(btnCancel);
            frmLogIn.Controls.Add(btnOk);
            txtUser.Focus();
        }
        public string LogInShow(string user = "")
        {
            CreateLogInForm(user);
            frmLogIn.ShowDialog();
            return userName;
        }
        void btnOkLoginClick(object sender, EventArgs e,string user,string password)
        {
            userPassword = password;
            userName = user.ToUpper();
            frmLogIn.Close();
        }
        void btnCancelLoginClick(object sender, EventArgs e)
        {
            userPassword = "";
            userName = "";
            frmLogIn.Close();
        }
        void CreateInputBox(string message)
        {
            inputBox = new Form();
            Button btnOk = new Button();
            Button btnCancel = new Button();
            Label lblMessage = new Label();
            btnOk.Location = new Point(60, 120);
            btnCancel.Location = new Point(20, 120);
            lblMessage.Location = new Point(10, 10);
            btnOk.Text = "OK";
            btnCancel.Text = "Cancel";
            lblMessage.Text = message;
            inputBox.Size = new Size(400, 200);
            btnOk.Click += new EventHandler(btnOkClick);
            btnCancel.Click += new EventHandler(btnCancelClick);
            inputBox.Controls.Add(btnOk);
            inputBox.Controls.Add(btnCancel);
            inputBox.Controls.Add(lblMessage);
        }
        void btnOkClick(object sender, EventArgs e)
        {
            inpuBoxPressed = "OK";
            inputBox.Close();
        }
        void btnCancelClick(object sender, EventArgs e)
        {
            inpuBoxPressed = "CANCEL";
            inputBox.Close();
        }
        public string MessageBoxShow(string message="")
        {
            CreateInputBox(message);
            inputBox.ShowDialog();
            return inpuBoxPressed;
        }
        public void CreateMessageBox()
        {

        }

        //CTCOIN
        public string CtCoinCommandConstructor(string command)
        {
            return "02" + (command.Length / 2).ToString("00") + command + CtCoinCRCCalc(command) + "03";
        }
        public string CtCoinGetMachineStatus()
        {
            return CtCoinCommandConstructor("1133");
        }

        public string CtCoinGetCurrencyCode()
        {
            return CtCoinCommandConstructor("1139");
        }

        public string CtCoinGetInfo()
        {
            return CtCoinCommandConstructor("1141");
        }

        public string CtCoinSetKeyboardInputMode()
        {
            return CtCoinCommandConstructor("35");
        }

        public string CtCoinGetKeyboardBuffer()
        {
            return CtCoinCommandConstructor("1121");
        }

        /// <summary>
        /// Command to simulate the key pressing on CtCoin keypad
        /// </summary>
        /// <param name="key">0-9 / ENTER / SETUP / CLEAR / REJECT / M+ / . / < / > / LOCK / MR / PRINT / TUBING / STARTSTOP / PROGRAMMING </param>
        /// <returns></returns>
        public string CtCoinKeyPress(string key)
        {
            string keyCode = "";
            switch (key.ToUpper())
            {
                case "ENTER":
                    keyCode = "0D";
                    break;
                case "CLEAR":
                    keyCode = "18";
                    break;
                case "REJECT":
                    keyCode = "20";
                    break;
                case "M+":
                    keyCode = "2B";
                    break;
                case ".":
                    keyCode = "2E";
                    break;
                case "0":
                case "1":
                case "2":
                case "3":
                case "4":
                case "5":
                case "6":
                case "7":
                case "8":
                case "9":
                    keyCode = Ascii2Hex(key);
                    break;
                case "<":
                    keyCode = "3C";
                    break;
                case ">":
                    keyCode = "3E";
                    break;
                case "LOCK":
                    keyCode = "4C";
                    break;
                case "MR":
                    keyCode = "4D";
                    break;
                case "PRINT":
                    keyCode = "50";
                    break;
                case "STARTSTOP":
                case "START":
                case "STOP":
                    keyCode = "53";
                    break;
                case "TUBING":
                    keyCode = "54";
                    break;
                case "PROGRAMMING":
                case "PROGRAM":
                    keyCode = "70";
                    break;
                case "SETUP":
                    keyCode = "71";
                    break;
            }
            return CtCoinCommandConstructor("215E" + keyCode);
        }

        public string CtCoinGetDisplayContent()
        {
            return CtCoinCommandConstructor("31");
        }
        
        public string CtCoinConnect()
        {
            return CtCoinCommandConstructor("013639333930323734");
        }

        public string CtCoinDisconnect()
        {
            return CtCoinCommandConstructor("03");
        }




        //CCTALK
        //mySerial.SendCommand(myFunc.CcTalkCommand(myCc3R.coinHoppers.hoppersDx[i].address, 0, 1, "F1")); //Request Software version

        public string CcTalkGetHopperStatus(int destAddress)
        {
            return CcTalkCommand(destAddress, 1, "A6");
        }
        public string CcTalkGetSerialNumber(int destAddress)
        {
            return CcTalkCommand(destAddress, 1, "F2"); 
        }
        public string CcTalkGetDiameters(int destAddress)
        {
            return CcTalkCommand(destAddress, 1, "29");
        }
        public string CcTalkGetProdCode(int destAddress)
        {
            return CcTalkCommand(destAddress, 1, "F4");
        }
        public string CcTalkGetManufID(int destAddress)
        {
            return CcTalkCommand(destAddress, 1, "F6");
        }
        public string CcTalkGetEquipCat(int destAddress)
        {
            return CcTalkCommand(destAddress, 1, "F5");
        }
        public string CcTalkAddressPoll( int destAddress = 0)
        {
            //Destination - n° data bytes - source - header - Data x - checksum
            return CcTalkCommand(destAddress, 1, "FD");
        }

        

        public STRUCTURES.SDConAnswer DConAnswer(string answer)
        {
            STRUCTURES.SDConAnswer commandAnswer = new STRUCTURES.SDConAnswer();
            if (answer.Length > 0)
            {
                commandAnswer.lead =Hex2Ascii(answer.Substring(0, 2));
                commandAnswer.moduleAddress = Hex2Ascii(answer.Substring(2, 4));
                //commandAnswer.data = Hex2Ascii(answer.Substring(6, answer.Length - 12));
                commandAnswer.chk = Hex2Ascii(answer.Substring(answer.Length - 6, 2)) + Hex2Ascii(answer.Substring(answer.Length - 4, 2));
                commandAnswer.cr = Hex2Ascii(answer.Substring(answer.Length - 2, 2));
            }
            return commandAnswer;
        }
        public STRUCTURES.SCCTalkAnswer CcTalkAnswer(string answer)
        {
            STRUCTURES.SCCTalkAnswer commandAnswer = new STRUCTURES.SCCTalkAnswer();
            if (answer.Length > 0)
            {
                try
                {
                    commandAnswer.destinationAddress = Hex2Ascii(answer.Substring(0, 2));
                    commandAnswer.bytes = Convert.ToInt16(answer.Substring(2, 2));
                    commandAnswer.sourceAddress = answer.Substring(4, 2);
                    commandAnswer.header = answer.Substring(6, 2);
                    commandAnswer.checksum = answer.Substring(8, 2);
                    commandAnswer.answer = answer.Substring(10, answer.Length - 10);
                    if (commandAnswer.answer.Length > 10)
                    {
                        commandAnswer.data = commandAnswer.answer.Substring(8,commandAnswer.answer.Length - 10);
                    }
                }
                catch 
                {

                }
            }
            return commandAnswer;
        }
        public string CcTalkEnableHopper(int destAddress)
        {
            string command = CcTalkCommand(destAddress, 1, "A4", "A5");
            return command;
        }
        public string CcTalkDispense(STRUCTURES.SCC3RHopper hopper, string hopperSerial)//int destAddress,
        {
            string request="";
            for (int i = 0; i<hopper.diameter.Length;i++)
            {
                request += "0001";
            }
            string command = CcTalkCommand(hopper.address, 1, "20", hopperSerial + request);
            return command;
        }
        public string CcTalkEmergencyStop(int destAddress)
        {
            string command = CcTalkCommand(destAddress, 1, "AC");
            return command;
        }
        public string CcTalkPaymentState(int destAddress)
        {
            string command = CcTalkCommand(destAddress, 1, "13");
            return command;
        }

        public string CcTalkCommand(int destination, int source, string cmd, string parameter = "")
        {
            string[] command = new string[4 + parameter.Length/2];
            string chk = "";
            int temp = 0;
            int bytes = parameter.Length / 2;
            command[0] = Dec2Hex(destination, 2);
            command[1] = Dec2Hex(bytes, 2);
            command[2] = Dec2Hex(source, 2);
            command[3] = cmd;
            for (int i = 4; i< command.Length;i++)
            {
                command[i] = parameter.Substring((i - 4) * 2, 2);
            }
            for (int i = 0; i < command.Length; i++)
            {
                temp += Convert.ToInt16(Hex2Dec(command[i]));
            }
            temp = 256 - temp;
            chk = Dec2Hex(temp, 2);
            cmd = command[0] + command[1] + command[2] + command[3] + parameter + chk.Substring(chk.Length-2,2);
            return cmd;
        }
        public string DConCommand(string cmd)
        {
            string command = "";
            string chk = "";
            int temp = 0;
            for (int i = 0; i < cmd.Length; i++)
            {
                command += Ascii2Hex(cmd.Substring(i, 1)).ToUpper();

            }
            for (int i = 0; i < command.Length; i = i + 2)
            {
                temp += Convert.ToInt16(Hex2Dec(command.Substring(i, 2)));
            }
            chk = Dec2Hex(temp);
            chk = Ascii2Hex(chk.Substring(chk.Length - 2, 1)) + Ascii2Hex(chk.Substring(chk.Length - 1, 1));
            command += chk.Substring(chk.Length - 4, 4);
            command += "0D";
            return command;
        }
        public string DConCommand(string leadChr, int moduleAddress, string cmd)
        {
            cmd = leadChr + Dec2Hex(moduleAddress, 2) + cmd;
            string command = "";
            string chk = "";
            int temp = 0;
            for (int i = 0; i < cmd.Length; i++)
            {
                command += Ascii2Hex(cmd.Substring(i, 1)).ToUpper();

            }
            for (int i = 0; i < command.Length; i = i + 2)
            {
                temp += Convert.ToInt16(Hex2Dec(command.Substring(i, 2)));
            }
            chk += Ascii2Hex(Dec2Hex(temp, 2).Substring(0, 1)) + Ascii2Hex(Dec2Hex(temp, 2).Substring(1, 1));
            command += chk.Substring(chk.Length - 4, 4);
            command += "0D";
            return command;
        }
        public string CtCoinCRCCalc(string command)
        {
            //byte[] data = Encoding.ASCII.GetBytes(command);
            string crcCtCoin = "";
            byte[] data = new byte[command.Length/2];
            for (int x = 0; x<command.Length/2;x++)
            {
                data[x] =Convert.ToByte(Hex2Dec(command.Substring(x*2, 2)));
            }
            int crc = 0;
            for (int i=0;i<data.Length;i++)
            {
                int val = data[i];
                crc ^= ((ushort)(val) << 8);
                for (int n=0;n<8;n++)
                {
                    if ((crc & 0x8000) != 0)
                        crc = (crc << 1) ^ 0x1021;
                    else
                        crc = (crc << 1);
                }
            }
            crcCtCoin =Dec2Hex(crc);
            crcCtCoin = crcCtCoin.Substring(crcCtCoin.Length - 4);
            return crcCtCoin; // il crc sono gli ultimi due byte (anche gli zeri vanno passati per il calcolo)
        }
        public string KeyPadBCCCalc(string command)
        {
            string bccKeyPad = "";
            int temp = 0;
            for (int x = 1; x < command.Length / 2; x++)
            {
                temp += Hex2Dec(command.Substring(x * 2, 2));
            }

            temp = temp & 0x7F;
            bccKeyPad = Dec2Hex(temp,2);
            return bccKeyPad;
        }
        public string KeyPadReset()
        {
            string command = "02312030303003";
            command += KeyPadBCCCalc(command);
            return command;
        }
        public string KeyPadBacklight(bool backlight=true)
        {
            string command = "";
            if (backlight == true)
                command = "0231213030313103";
            else
                command = "0231213030313003";
            command += KeyPadBCCCalc(command);
            return command;
        }
        public string KeyPadCursorType(int cursorTypeID = 0)
        {
            string command = "023122303031" + Ascii2Hex(cursorTypeID.ToString()) + "03";
            command += KeyPadBCCCalc(command);
            return command;
        }
        public string KeyPadConstrast(string value)
        {
            string command = "023124303031" + Ascii2Hex(value) + "03";
            command += KeyPadBCCCalc(command);
            return command;
        }
        public string KeyPadWriter(string value)
        {
            string command = "023123" + Ascii2Hex(value.Length.ToString("000")) +  Ascii2Hex(value) + "03";
            command += KeyPadBCCCalc(command);
            return command;
        }

        public string WaitingKey(string specificKey = "")
        {
            keyCodePressed = 0;
            Application.DoEvents();
            bool pressed = false;
            int specificValue = 1000;
            string key = "";
            switch (specificKey)
            {
                case "ENTER":
                case "INVIO":
                    specificValue = 13;
                    break;
                case "ESC":
                    specificValue = 27;
                    break;
                default:
                    if (specificKey.Length > 0)
                        specificValue = Convert.ToChar(specificKey);
                    break;
            }
            do
            {
                System.Windows.Forms.Application.DoEvents();
                if (specificValue == 1000 && keyCodePressed != 0)
                { pressed = true; }
                if (specificValue == keyCodePressed)
                { pressed = true; }
            } while (pressed == false);

            switch (keyCodePressed)
            {
                case 13:
                    key = "ENTER";
                    break;
                case 27:
                    key = "ESC";
                    break;
                case 32:
                    key = "SPACE";
                    break;
                case 82:
                case 114:
                    key = "R";
                    break;
                case 76:
                case 108:
                    key = "L";
                    break;
                default:
                    key = Convert.ToChar(keyCodePressed).ToString();
                    break;
            }
            keyCodePressed = 0;
            return key;
        }
        public void WriteMessage(System.Windows.Forms.Label label, string message, Color Colors = default(Color))
        {
            label.Text = message;
            //if (Colors == default(Color)) { Colors=Color.Green; }
            label.ForeColor = Colors;
            label.Refresh();
        }
        public void WriteMessage(System.Windows.Forms.Label label, int messageID, Color Colors = default(Color))
        {
            label.Text = iniMessage.GetValue("messages", "msg" + messageID);
            if (Colors == default(Color)) { Colors = Color.White; }
            label.ForeColor = Colors;
            label.Refresh();
        }
        public void WriteMessage(System.Windows.Forms.Label label, int messageID, Color Colors = default(Color), params object[] param)
        {
            label.Text = string.Format(iniMessage.GetValue("messages", "msg" + messageID), param);
            //if (Colors == default(Color)) { Colors = Color.Green; }
            label.ForeColor = Colors;
            label.Refresh();
        }
        public void WaitingTime(int mSec = 1000)
        {
            System.Windows.Forms.Application.DoEvents();
            System.Threading.Thread.Sleep(mSec);
        }
        public void LogWrite(string fileName, string logBlock, bool endLog = false)
        {
            StreamWriter logFile = File.AppendText(fileName);
            if (endLog == true)
            {
                logFile.WriteLine(logBlock);
            }
            else
            {
                logFile.Write(logBlock);
            }
            logFile.Close();
        }
        public void SendLog(string localFile, string serverFolder)
        {
            if (File.Exists(localFile))
            {
                if (serverFolder.Substring(serverFolder.Length - 1) != "\\") { serverFolder += "\\"; }
                try
                {
                    StreamReader srLocalFile = new StreamReader(localFile);
                    StreamWriter serverFile = new StreamWriter(serverFolder + localFile, append: true);
                    serverFile.Write(srLocalFile.ReadToEnd());
                    serverFile.Close();
                    srLocalFile.Close();
                    File.Delete(localFile);
                }
                catch (System.IO.IOException e)
                {
                    string answer = e.Message;
                }
            }
        }
        #region HEX / BIN / DEC convertions
        /// <summary>
        /// Convert a HEX value to a DEC value
        /// </summary>
        /// <param name="value">HEX value</param>
        /// <returns></returns>
        public int Hex2Dec(string value)
        {
            value = value.ToUpper();
            int result = 0;
            string temp = "";
            int molt = 0;
            if (value != "NO ANSWER")
            {
                for (int i = 1; i <= value.Length; i++)
                {
                    temp = value.Substring(value.Length - i, 1);
                    switch (temp)
                    {
                        case "A":
                            molt = 10;
                            break;
                        case "B":
                            molt = 11;
                            break;
                        case "C":
                            molt = 12;
                            break;
                        case "D":
                            molt = 13;
                            break;
                        case "E":
                            molt = 14;
                            break;
                        case "F":
                            molt = 15;
                            break;
                        default:
                            molt = Convert.ToInt16(temp);
                            break;
                    }
                    result += Convert.ToInt16(Math.Pow(16, i - 1) * molt);
                }
            }
            return result;
        }
        public double Hex2Dbl(string value)
        {
            value = value.ToUpper();
            double result = 0;
            string temp = "";
            double molt = 0;
            if (value != "NO ANSWER")
            {
                for (int i = 1; i <= value.Length; i++)
                {
                    temp = value.Substring(value.Length - i, 1);
                    switch (temp)
                    {
                        case "A":
                            molt = 10;
                            break;
                        case "B":
                            molt = 11;
                            break;
                        case "C":
                            molt = 12;
                            break;
                        case "D":
                            molt = 13;
                            break;
                        case "E":
                            molt = 14;
                            break;
                        case "F":
                            molt = 15;
                            break;
                        default:
                            molt = Convert.ToInt16(temp);
                            break;
                    }
                    result += Convert.ToDouble(Math.Pow(16, i - 1) * molt);
                }
            }
            return result;
        }
        /// <summary>
        /// Convert a HEX value to a BIN value
        /// </summary>
        /// <param name="value">HEX value</param>
        /// <param name="bit">Number of bit (considered only if highter of number of bit calculated)</param>
        /// <returns></returns>
        public string Hex2Bin(string value, int bit = 0)
        {
            value = value.ToUpper();
            string result = "";
            string temp = "";
            if (value != "NO ANSWER")
            {
                for (int i = 1; i <= value.Length; i++)
                {
                    temp = value.Substring(i - 1, 1);
                    switch (temp)
                    {
                        case "0":
                            temp = "0000";
                            break;
                        case "1":
                            temp = "0001";
                            break;
                        case "2":
                            temp = "0010";
                            break;
                        case "3":
                            temp = "0011";
                            break;
                        case "4":
                            temp = "0100";
                            break;
                        case "5":
                            temp = "0101";
                            break;
                        case "6":
                            temp = "0110";
                            break;
                        case "7":
                            temp = "0111";
                            break;
                        case "8":
                            temp = "1000";
                            break;
                        case "9":
                            temp = "1001";
                            break;
                        case "A":
                            temp = "1010";
                            break;
                        case "B":
                            temp = "1011";
                            break;
                        case "C":
                            temp = "1100";
                            break;
                        case "D":
                            temp = "1101";
                            break;
                        case "E":
                            temp = "1110";
                            break;
                        case "F":
                            temp = "1111";
                            break;
                    }
                    result += temp;
                }

                if (bit > result.Length)
                {
                    for (int i = result.Length; i < bit; i++)
                    {
                        result = "0" + result;
                    }
                }
            }
            return result;
        }
        /// <summary>
        /// Convert DEC number (int) to ASCII value
        /// </summary>
        /// <param name="value">DEC number to convert in ascci value</param>
        /// <returns></returns>
        public string Dec2Ascii(int value)
        {
            string result = "";
            switch (value)
            {
                case 2:
                    result = "SOT";
                    break;
                case 3:
                    result = "EOT";
                    break;
                case 5:
                    result = "ENQ";
                    break;
                case 6:
                    result = "ACK";
                    break;
                case 10:
                    result = "LF";
                    break;
                case 13:
                    result = "ENTER";
                    break;
                case 21:
                    result = "NAK";
                    break;
                case 24:
                    result = "CANCEL";
                    break;
                case 27:
                    result = "ESC";
                    break;
                case 32:
                    result = "SPACE";
                    break;

                default:
                    char c = (char)value;
                    result = c.ToString();
                    break;
            }

            return result;
        }

        /// <summary>
        /// Convert DEC number (int) to HEX value
        /// </summary>
        /// <param name="value">DEC (int) number</param>
        /// <param name="bit">Number of bit (considered only if highter of number of bit calculated)</param>
        /// <returns></returns>
        public string Dec2Hex(int value, int bit = 0)
        {
            string result = value.ToString("X" + bit);
            return result;
        }
        /// <summary>
        /// Convert DEC number (int) to HEX value
        /// </summary>
        /// <param name="value">DEC 8int) number</param>
        /// <param name="bit">Number of bit (considered only if highter of number of bit calculated)</param>
        /// <returns></returns>
        public string Dec2Bin(int value, int bit = 0)
        {
            string result = Convert.ToString(value, 2);
            if (bit > result.Length)
            {
                for (int i = result.Length; i < bit; i++)
                {
                    result = "0" + result;
                }
            }
            return result;
        }
        /// <summary>
        /// Convert BIN value to a DEC number (int)
        /// </summary>
        /// <param name="value">BIN value</param>
        /// <returns></returns>
        public int Bin2Dec(string value)
        {
            int result = 0;
            int temp = 0;

            for (int i = 0; i < value.Length; i++)
            {
                temp = Convert.ToInt16(value.Substring(value.Length - (i + 1), 1));
                if (temp == 1)
                {
                    temp = Convert.ToInt16(Math.Pow(2, i));
                }
                result += temp;
            }

            return result;
        }
        public string Bin2Hex(string value, int bit = 0)
        {
            int temp = Bin2Dec(value);
            string result = Dec2Hex(temp, bit);
            return result;
        }
        public int Hex2AsciiDec(string value)
        {
            int result = 0;
            if (value != "NO ANSWER")
            {
                string temp = string.Empty;
                for (int i = 0; i < value.Length; i += 2)
                {
                    string char2Convert = value.Substring(i, 2);
                    int n = Convert.ToInt32(char2Convert, 16);
                    char c = (char)n;
                    temp += c.ToString();
                }
                result = Convert.ToInt16(temp);
            }
            return result;
        }
        public string Hex2Ascii(string value)
        {
            string result = string.Empty;
            if (value != "NO ANSWER")
            {
                for (int i = 0; i < value.Length; i += 2)
                {
                    string char2Convert = value.Substring(i, 2);
                    int n = Convert.ToInt32(char2Convert, 16);
                    char c = (char)n;
                    result += c.ToString();
                }
            }
            return result;
        }
        public string Ascii2Hex(string value)
        {
            StringBuilder sb = new StringBuilder();
            byte[] inputBytes = Encoding.UTF8.GetBytes(value);
            foreach (byte b in inputBytes)
            {
                sb.Append(string.Format("{0:x2}", b).ToUpper());
            }
            return sb.ToString();
        }
        #endregion
    }
    public class RS232
    {
        public SerialPort mySerial = new SerialPort();
        public Stopwatch myTimer = new Stopwatch();
        public string answerString;
        public byte[] answerByte;
        public string commandAnswer = "";
        public string commandSent = "";
        public FUNCTIONS myFunc = new FUNCTIONS();

        public Form prova = new Form();
        public event EventHandler DataReceived;
        public event EventHandler DataSent;

        /// <summary>
        /// Instance creation
        /// </summary>
        public RS232()
        {
            Initialize();

        }

        /// <summary>
        /// Instance creation specifying the com port name
        /// </summary>
        /// <param name="comPortName">Name of the COM port (ex. 'COM1')</param>
        public RS232(string comPortName)
        {
            Initialize();
            ConfigCom(comPortName);
        }

        void Initialize()
        {
            myTimer.Reset();
        }

        /// <summary>
        /// Open the COM port if closed
        /// </summary>
        public void OpenCom()
        {
            if (!mySerial.IsOpen) mySerial.Open();
        }

        /// <summary>
        /// Close the COM port if opened
        /// </summary>
        public void CloseCom()
        {
            if (mySerial.IsOpen) mySerial.Close();
        }

        /// <summary>
        /// Set COM port parameters
        /// </summary>
        /// <param name="portName">Name of the port (ex. 'COM1')</param>
        /// <param name="baudrate">Baudrate (ex. 9600)</param>
        /// <param name="parity">Parity control (ex. Parity.None)</param>
        /// <param name="dataBits">Number of data bits (ex. 8)</param>
        /// <param name="stopBits">Number of stop bits (ex. StopBits.One)</param>
        public void ConfigCom(string portName, int baudrate = 9600, Parity parity = Parity.None, int dataBits = 8, StopBits stopBits = StopBits.One)
        {
            portName = portName.ToUpper();
            if (portName.Substring(0, 3) != "COM")
            {
                portName = "COM" + portName;
            }
            mySerial.PortName = portName;
            mySerial.BaudRate = baudrate;
            mySerial.Parity = parity;
            mySerial.DataBits = dataBits;
            mySerial.StopBits = stopBits;
        }

        public string SendCommand(string command, int nByteAnswer = 1, int timeOut = 5000, bool autoClose = false)
        {
            if (mySerial.IsOpen == false) { mySerial.Open(); }
            if (command.Length % 2 != 0) { command = "0" + command; }

            byte[] commandToSend = new byte[command.Length / 2];
            int index = 0;
            commandAnswer = "";
            int bytesReceived = 0;
            for (int i = 1; i < command.Length; i += 2)
            {
                commandToSend[index] = Convert.ToByte(command.Substring(i - 1, 2), 16);
                index += 1;
            }
            mySerial.DiscardInBuffer();
            DataSent(command, null);
            mySerial.Write(commandToSend, 0, command.Length / 2);
            myFunc.WaitingTime(250);
            myTimer.Restart();
            if (nByteAnswer > 0)
            {
                do
                {
                    if (myTimer.ElapsedMilliseconds > timeOut)
                    {
                        commandAnswer = "NO ANSWER";
                        goto endCommand;
                    }
                    bytesReceived = mySerial.BytesToRead;
                    myFunc.WaitingTime(100);
                } while (bytesReceived < nByteAnswer);
                byte[] dataReceived = new byte[bytesReceived];
                mySerial.Read(dataReceived, 0, bytesReceived);
                for (int i = 0; i < dataReceived.Length; i++)
                {
                    commandAnswer += myFunc.Dec2Hex(dataReceived[i], 2);
                }
            }
            else
            {
                myFunc.WaitingTime(timeOut);
            }
        endCommand:
            myTimer.Reset();
            DataReceived(commandAnswer, null);
            if (autoClose == true & mySerial.IsOpen == true) { mySerial.Close(); }
            return commandAnswer;
        }

        


    }
    public class INIFILE
    {

        string EXE = Assembly.GetExecutingAssembly().GetName().Name;
        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string Section, string Key, string Value, string FilePath);

        public void SetValue(string section, string key, string value)
        {
            WritePrivateProfileString(section ?? EXE, key, value, Application.StartupPath + "\\" + theFile);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IniFile"/> class.
        /// </summary>
        /// <param name="file">The initialization file path.</param>
        /// <param name="commentDelimiter">The comment delimiter string (default value is ";").
        /// </param>
        public INIFILE(string file, string commentDelimiter = ";")
        {
            CommentDelimiter = commentDelimiter;
            TheFile = file;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IniFile"/> class.
        /// </summary>
        public INIFILE()
        {
            CommentDelimiter = ";";
        }

        /// <summary>
        /// The comment delimiter string (default value is ";").
        /// </summary>
        public string CommentDelimiter { get; set; }

        private string theFile = null;

        /// <summary>
        /// The initialization file path.
        /// </summary>
        public string TheFile
        {
            get
            {
                return theFile;
            }
            set
            {
                theFile = null;
                dictionary.Clear();
                if (File.Exists(value))
                {
                    theFile = value;
                    using (StreamReader sr = new StreamReader(theFile))
                    {
                        string line, section = "";
                        while ((line = sr.ReadLine()) != null)
                        {
                            line = line.Trim();
                            if (line.Length == 0) continue;  // empty line
                            if (!String.IsNullOrEmpty(CommentDelimiter) && line.StartsWith(CommentDelimiter))
                                continue;  // comment

                            if (line.StartsWith("[") && line.Contains("]"))  // [section]
                            {
                                int index = line.IndexOf(']');
                                section = line.Substring(1, index - 1).Trim();
                                continue;
                            }

                            if (line.Contains("="))  // key=value
                            {
                                int index = line.IndexOf('=');
                                string key = line.Substring(0, index).Trim();
                                string val = line.Substring(index + 1).Trim();
                                string key2 = String.Format("[{0}]{1}", section, key).ToLower();

                                if (val.StartsWith("\"") && val.EndsWith("\""))  // strip quotes
                                    val = val.Substring(1, val.Length - 2);

                                if (dictionary.ContainsKey(key2))  // multiple values can share the same key
                                {
                                    index = 1;
                                    string key3;
                                    while (true)
                                    {
                                        key3 = String.Format("{0}~{1}", key2, ++index);
                                        if (!dictionary.ContainsKey(key3))
                                        {
                                            dictionary.Add(key3, val);
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    dictionary.Add(key2, val);
                                }
                            }
                        }
                    }
                }
            }
        }

        // "[section]key"   -> "value1"
        // "[section]key~2" -> "value2"
        // "[section]key~3" -> "value3"
        private Dictionary<string, string> dictionary = new Dictionary<string, string>();

        private bool TryGetValue(string section, string key, out string value)
        {
            string key2;
            if (section.StartsWith("["))
                key2 = String.Format("{0}{1}", section, key);
            else
                key2 = String.Format("[{0}]{1}", section, key);

            return dictionary.TryGetValue(key2.ToLower(), out value);
        }

        /// <summary>
        /// Gets a string value by section and key.
        /// </summary>
        /// <param name="section">The section.</param>
        /// <param name="key">The key.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns>The value.</returns>
        /// <seealso cref="GetAllValues"/>
        public string GetValue(string section, string key, string defaultValue = "")
        {
            string value;
            if (!TryGetValue(section, key, out value))
                return defaultValue;

            return value;
        }

        /// <summary>
        /// Gets a string value by section and key.
        /// </summary>
        /// <param name="section">The section.</param>
        /// <param name="key">The key.</param>
        /// <returns>The value.</returns>
        /// <seealso cref="GetValue"/>
        public string this[string section, string key]
        {
            get
            {
                return GetValue(section, key);
            }
        }

        /// <summary>
        /// Gets an integer value by section and key.
        /// </summary>
        /// <param name="section">The section.</param>
        /// <param name="key">The key.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <param name="minValue">Optional minimum value to be enforced.</param>
        /// <param name="maxValue">Optional maximum value to be enforced.</param>
        /// <returns>The value.</returns>
        public int GetInteger(string section, string key, int defaultValue = 0,
            int minValue = int.MinValue, int maxValue = int.MaxValue)
        {
            string stringValue;
            if (!TryGetValue(section, key, out stringValue))
                return defaultValue;

            int value;
            if (!int.TryParse(stringValue, out value))
            {
                double dvalue;
                if (!double.TryParse(stringValue, out dvalue))
                    return defaultValue;
                value = (int)dvalue;
            }

            if (value < minValue)
                value = minValue;
            if (value > maxValue)
                value = maxValue;
            return value;
        }

        /// <summary>
        /// Gets a double floating-point value by section and key.
        /// </summary>
        /// <param name="section">The section.</param>
        /// <param name="key">The key.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <param name="minValue">Optional minimum value to be enforced.</param>
        /// <param name="maxValue">Optional maximum value to be enforced.</param>
        /// <returns>The value.</returns>
        public double GetDouble(string section, string key, double defaultValue = 0,
            double minValue = double.MinValue, double maxValue = double.MaxValue)
        {
            string stringValue;
            if (!TryGetValue(section, key, out stringValue))
                return defaultValue;

            double value;
            if (!double.TryParse(stringValue, out value))
                return defaultValue;

            if (value < minValue)
                value = minValue;
            if (value > maxValue)
                value = maxValue;
            return value;
        }

        /// <summary>
        /// Gets a boolean value by section and key.
        /// </summary>
        /// <param name="section">The section.</param>
        /// <param name="key">The key.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns>The value.</returns>
        public bool GetBoolean(string section, string key, bool defaultValue = false)
        {
            string stringValue;
            if (!TryGetValue(section, key, out stringValue))
                return defaultValue;

            return (stringValue != "0" && !stringValue.StartsWith("f", true, null));
        }

        /// <summary>
        /// Gets an array of string values by section and key.
        /// </summary>
        /// <param name="section">The section.</param>
        /// <param name="key">The key.</param>
        /// <returns>The array of values, or null if none found.</returns>
        /// <seealso cref="GetValue"/>
        public string[] GetAllValues(string section, string key)
        {
            string key2, key3, value;
            if (section.StartsWith("["))
                key2 = String.Format("{0}{1}", section, key).ToLower();
            else
                key2 = String.Format("[{0}]{1}", section, key).ToLower();

            if (!dictionary.TryGetValue(key2, out value))
                return null;

            List<string> values = new List<string>();
            values.Add(value);
            int index = 1;
            while (true)
            {
                key3 = String.Format("{0}~{1}", key2, ++index);
                if (!dictionary.TryGetValue(key3, out value))
                    break;
                values.Add(value);
            }

            return values.ToArray();
        }
    }
    public class CMDLL
    {

        public event EventHandler CommandSent;
        public event EventHandler CommandReceived;

        #region CONSTANTS
        //CONSTANTS
        public const int COMM_TIMER_SECOND5 = 5000;
        public const int QUIT_THREAD = 2;
        public const int THREAD_MESSAGES = 3;
        public const int OPEN_POPUP_MENU = 4;
        public const int PROT_TRANSPARENT_OFF = 0;
        public const int PROT_TRANSPARENT_ON = 1;
        public const int PROTO_JOB_STATUS_READY = 2;
        public const int PROTO_JOB_STATUS_SEND = 4;
        public const int PROTO_JOB_STATUS_RECEIVE = 6;
        public const int PROTO_JOB_STATUS_RECEIVE_DONE = 7;
        public const int PROTO_JOB_STATUS_TRANSPARENT = 8;
        public const int PROTO_JOB_STATUS_RECEIVE_STX = 10;
        public const int PROTO_JOB_STATUS_RECEIVE_ETB_OR_ETX = 11;
        public const int PROTO_JOB_STATUS_RECEIVE_DLE = 12;
        public const int PROTO_JOB_STATUS_RECEIVE_EOT = 13;
        public const int PROTO_JOB_STATUS_RECEIVE_BCC = 14;
        public const int PROTO_JOB_STATUS_RECEIVE_0_OR_1 = 15;
        public const int PROTO_JOB_STATUS_END = 19;
        public const int PROTO_JOB_STATUS_RECEIVE_TRANSPARENT = 20;
        public const int PROTO_JOB_STATUS_RECEIVE_ENQ = 21;
        public const int PROTO_JOB_STATUS_BLOCK = 22;
        public const int PROTO_JOB_STATUS_WAIT_TRANSPARENT = 23;
        public const int PROTO_JOB_STATUS_FILETRN = 24;

        public const int PROTO_TEXT_MAX_LEN = 80;

        public const int PROTO_CHAR_OP_SEPARATOR = 0x2C;

        public const int PROTO_CONTROL_CHAR_STX = 0x02;
        public const int PROTO_CONTROL_CHAR_ETX = 0x03;
        public const int PROTO_CONTROL_CHAR_EOT = 0x04;
        public const int PROTO_CONTROL_CHAR_ENQ = 0x05;
        public const int PROTO_CONTROL_CHAR_ACK = 0x06;
        public const int PROTO_CONTROL_CHAR_DLE = 0x10;
        public const int PROTO_CONTROL_CHAR_0 = 0x30;
        public const int PROTO_CONTROL_CHAR_1 = 0x31;
        public const int PROTO_CONTROL_CHAR_NAK = 0x15;
        public const int PROTO_CONTROL_CHAR_ETB = 0x17;

        public const string PROTO_CONTROL_STRING_DLE0 = "\x10\x30";
        public const string PROTO_CONTROL_STRING_DLE1 = "\x10\x31";
        public const int PROTO_CONTROL_STRING_DLE0_LEN = 2;
        public const int PROTO_CONTROL_STRING_DLE1_LEN = 2;

        public const int LINK_RECONNECTED = 69;

        public const byte DLINK_MODE_SERIAL = 83;       // connection using RS232 port
        public const byte DLINK_MODE_SERIAL_EASY = 115;    // connection using RS232 port with LAN protocol
        public const byte DLINK_MODE_TCPIP = 76;     // connection using TCP/IP port
        public const byte DLINK_MODE_SSL = 108;    // connection secure SSL
        public const byte DLINK_MODE_TLS = 116;    // connection secure TLS
        public const byte DLINK_MODE_USB = 85;     // connection using USB port
        public const byte DLINK_MODE_EMULATION = 69;     // connection in Emulation Moode
        public const byte DLINK_MODE_USB_EASY = 117;    // connection using USB port with USB protocol
        public const byte DLINK_MODE_BLOCKTRANSFERT = 70;     // connection for UpLoad file tranfert
        public const byte DLINK_MODE_SERVER_LAN = 101;  // connection server for emulator in lan


        //#define WM_EXECUTE_COMPLETE									WM_USER+601     // Notification message to end of the sending operation

        //----------API Return code-----------------------------------------------------
        public const int CM_OK = 0;// Command correctly executed
                                    //#define CM_ERROR                        0xFFFF // Generic Error

        public const int CM_LINK_SYSTEM_ERR = 1050;   //"CMLINK layer -Function communication device error"
        public const int CM_LINK_CONNECTION_ERR = 1051;   //"CMLINK layer -Invalid handle"
        public const int CM_LINK_ALREADY_CONNECTED_ERR = 1052;   //"CMLINK layer -Connection already connected"
        public const int CM_LINK_CONNECT_PARAMETERS_ERR = 1053;   //"CMLINK layer -Syntax error on connect operation"
        public const int CM_LINK_WRITE_ERR = 1054;   //"CMLINK layer -WriteFile Function on device has failed"
        public const int CM_LINK_READ_ERR = 1055;   //"CMLINK layer -ReadFile Function on device has failed"
        public const int CM_LINK_LAN_OPEN_ERR = 1056;   //"CMLINK layer -Error on TCP/IP socket connection routines"
        public const int CM_LINK_LAN_WRITE_ERR = 1057;   //"CMLINK layer -Write on LAN has failed"
        public const int CM_LINK_LAN_READ_ERR = 1058;   //"CMLINK layer -Read on LAN has failed"
        public const int CM_LINK_TIMEOUT_ERR = 1059;   //"CMLINK layer -Time command has expired"
        public const int CM_LINK_OVERFLOW_ERR = 1060;   //"CMLINK layer -Buffer of reply command has been exeed"
        public const int CM_LINK_BCC_ERR = 1061;   //"CMLINK layer -Trasmission error, wrong BCC/char"
        public const int CM_LINK_PROTOCOL_ERR = 1062;   //"CMLINK layer -Trasmission error, wrong protocol "
        public const int CM_LINK_CDM_IN_PROGRESS = 1063;   //"CMLINK layer -Command in progress"
        public const int CM_LINK_TERMINATE_EMULATOR = 1064;   //"CMLINK layer -Command in progress"
        public const int CM_LINK_SEQUENCE_ERR = 1065;   //"CMLINK layer -Command in progress"
        public const int CM_LINK_LAN_CONNECTION_REFUSED = 1066;   //"CMLINK layer -LAN Connection refused 10061"
        public const int CM_LINK_SYNTAX_ERR = 1067;   //"CMLINK Syntax Error"
        public const int CM_LINK_LAN_CONNECTION_RESET = 1068;   //"CMLINK layer -LAN 10054 - Connection reset by peer"
        public const int CM_LINK_LANSERVER_CLIENT_CLOSE = 1069;   //"CMLINK layer Close session by client in LanServer"

        public const int CM_COMMAND_SYNTAXERROR = 2000;   // "CM_COMMAND layer -Error in input parameter"
        public const int CM_COMMAND_INVALID_REPLY_PARAMETER = 2001;   // "CM_COMMAND layer -Output parameter cannot be NULL"
        public const int CM_COMMAND_UNEXPECTEDREPLY = 2002;   // "CM_COMMAND layer -Error in input parameter"
        public const int CM_COMMAND_REFUSED = 2003;   // "CM_COMMAND layer -"
        public const int CM_COMMAND_INVALID_CONNECT_HANDLE = 2004;  //"CM_COMMAND layer -System Error"

        public const byte TRANSPARENT = 35;     //#
        public const byte EMUL_RECEIVE = 64;     //@
        public const int CONNECTION = -1;     //&HFFFF
        public const byte TWS_CTS = 123;    //{
        public const byte EMUL_SEND = 93;     //]

        public const double CM_DEF_TIMEOUT = 0xFFFFFFFF;
        public const double CM_DEF_TIMEOUTx2 = 0xFFFFFFFE;
        public const double CM_DEF_TIMEOUTx3 = 0xFFFFFFFD;
        public const double CM_DEF_TIMEOUTx4 = 0xFFFFFFFC;

        public const short MAX_PATH = 260;
        public const byte LEN_PASSWORD = 6;
        public const byte MAX_SLOT_ACTIVE = 4;
        public const byte MAX_FLASH_REF = 16;
        public const byte MAX_CASSETTES = 24;
        public const byte MAX_DELAYCLASS = 10;
        public const byte MAX_USERID = 10;
        public const byte MAX_MODULE = 30;
        public const byte MAX_CHARDENOM = 4;
        public const byte MAX_CHANNEL = 32;
        public const byte MAX_DEPOSIT = 200;
        public const byte MAX_DENOMINATION = 32;
        public const byte MAX_BOOKING = 29;
        public const byte SERIALNUMBER = 12;
        public const double MAX_LOG = 8192;
        public const byte MAX_HOOPER_COIN = 8;
        public const byte NUM_PROTO_DM = 6;

        public const byte AUTOASSIGNCASSETTES = 65;     //'A'
        public const byte BELL = 66;     //'B'
        public const byte CLOSE = 67;     //'C'
        public const byte DEPOSIT_CASH = 68;     //'D'
        public const byte EXTENDED_STATUS = 69;     //'E'
        public const byte FILL = 70;     //'F'
        public const byte GET_CASH_DATA = 71;     //'G'
        public const byte ERROR_LOG = 72;     //'H'
        public const byte INIT = 73;     //'I'
        public const byte JOURNAL = 74;     //'J'
        public const byte KEYCHANGE = 75;     //'K'
        public const byte DOWNLOAD = 76;     //'L'
        public const byte OPEN = 79;     //'O'
        public const byte SINGLECMD = 91;     //'['
        public const byte FILETRANSF = 94;     //'^'
        public const byte WITHDRAWAL = 87;     //'W'
        public const byte SETSEVLEVEL = 80;     //'P'
        public const byte SETOIDPAR = 78;     //'N'
        public const byte GETSEVLEVEL = 81;     //'Q'
        public const byte GET_CONFIG = 82;     //'R'
        public const byte SETCONFIG = 83;     //'S'
        public const byte TEST = 84;     //'T'
        public const byte UNDO = 85;     //'U'
        public const byte VERSION = 86;     //'V'
        public const byte TRASPARENT_START = 88;     //'X'
        public const byte TRASPARENT_END = 89;     //'Y'
        public const byte MOD_FILL = 90;     //'Z'
        public const byte CTS_TWS = 121;    //'y'

        public const byte NewAssign = 97;     //'a'
        public const byte GET_CASH_MONSTER = 103;    //'g'
        public const byte EXT_STATUS_MONSTER = 101;    //'e'
        public const byte MOVENOTES = 119;    //'w'
        public const byte GET_CNF_MONSTER = 114;    //'r'
        public const byte SWAPNOTES = 115;    //'s'
        public const byte COIN_DSP = 100;    //'d'
        public const byte UTILITY = 117;    //'u'
        public const byte ROBBERY = 98;     //'b'
        public const byte MOD_DEP = 77;     //'M'

        #endregion

        #region VARIABLES
        public static short hCon;
        public static ConnectionParam ConnPar;
        public static short hTrace;
        public static string stTrace;
        public static UInt32 lastError;
        public static string commandSent;
        public string commandAns;
        public string[] commandSingleAns;
        public int n = 1;
        #endregion

        #region STRUCTURES


        public struct SReplyOpen
        {
            public string side;
            public string rc;
        }
        public static SReplyOpen replyOpen;

        public struct SCassetteInfo
        {
            public string noteId;
            public string bnNumber;
            public string bnFreeCap;
            public string name;
            public string enabled;
        }
        public static SCassetteInfo CassetteInfo;

        public struct SGetCashData
        {
            public string side;
            public string rc;
            public SCassetteInfo[] CassetteInfo;
            public string numCasProt;
        }
        public static SGetCashData replyGetCashData;

        public struct SNotesInfo
        {
            public string denomination;
            public int bnNum;
        }
        public static SNotesInfo NotesInfo;

        public struct SDeposit
        {
            public string side;
            public string rc;
            public int bnTotal;
            public int bnOutput;
            public int bnRej;
            public SNotesInfo[] depositInfo;
        }
        public static SDeposit replyDeposit;

        public struct SCounting
        {
            public string side;
            public string rc;
            public int bnAccepted;
            public int bnRej;
            public int bnSuspect;
            public SNotesInfo[] depositInfo;
        }
        public static SCounting replyCounting;

        public struct SUndo
        {
            public string side;
            public string rc;
            public SNotesInfo[] undoInfo;
        }
        public static SUndo replyUndo;

        public struct SWithdrawal
        {
            public string side;
            public string rc;
            public SNotesInfo[] noteInfo;
        }
        public static SWithdrawal replyWithdrawal;

        public struct SCommand
        {
            public string daVedere;
        }

        public struct STransferInfo
        {
            public string noteId;
            public int bnNum;
            public int bnDep;
        }

        public struct STransfer
        {
            public string side;
            public string rc;
            public STransferInfo[] transferInfo;
        }
        public static STransfer replyTranfer;

        public struct SExtendedStatus
        {
            public string rc;
            public string feederStatus;
            public string controllerStatus;
            public string readerStatus;
            public string safeStatus;
            public string cassetteAStatus;
            public string cassetteBStatus;
            public string cassetteCStatus;
            public string cassetteDStatus;
            public string cassetteEStatus;
            public string cassetteFStatus;
            public string cassetteGStatus;
            public string cassetteHStatus;
            public string cassetteIStatus;
            public string cassetteJStatus;
            public string cassetteKStatus;
            public string cassetteLStatus;
            public string escrowQStatus;
            public string depositAStatus;
            public string escrowRStatus;
            public string depositBStatus;
        }
        public static SExtendedStatus replyExtendedStatus;

        public struct SFirmware
        {
            public string ver;
            public string rel;
        }

        public struct SVersion
        {
            public string rc;
            public SFirmware controller;
            public SFirmware feeder;
            public SFirmware realTime;
            public SFirmware reader;
            public SFirmware safe;
            public SFirmware FPGA;
            public SFirmware opSystem;
            public SFirmware cassetteA;
            public SFirmware cassetteB;
            public SFirmware cassetteC;
            public SFirmware cassetteD;
            public SFirmware cassetteE;
            public SFirmware cassetteF;
            public SFirmware cassetteG;
            public SFirmware cassetteH;
            public SFirmware cassetteI;
            public SFirmware cassetteJ;
            public SFirmware cassetteK;
            public SFirmware cassetteL;
            public SFirmware depositA;
            public SFirmware depositB;
        }
        public static SVersion replyVersion;

        public struct SAssign
        {
            public string rc;
            public string message;
        }
        public static SAssign replyAssign;

        public struct SRcOnly
        {
            public string rc;
        }
        public static SRcOnly replySetDateTime;
        public static SRcOnly replyFillCmConfig;
        public static SRcOnly replyFillCmOptionConfig;
        public static SRcOnly replyFillCmOptionOneConfig;
        public static SRcOnly replyFillCmOptionTwoConfig;
        public static SRcOnly replyFillCmUnitIdentification;
        public static SRcOnly replyFillCmCassetteNumber;
        public static SRcOnly replyFillWithdrawallBundleSize;
        public static SRcOnly replyFillCountingBundleSize;
        public static SRcOnly replyFillCmProtCassNumber;
        public static SRcOnly replyFillCD80CassNumber;

        public struct SUnitCoverTest
        {
            public string rc;
            public string safeDoor;
            public string cover;
            public string feeder;
            public string inputSlot;
            public string rejectSlot;
            public string leftSlot;
            public string rightSlot;
        }
        public static SUnitCoverTest replyUnitCoverTest;

        public struct STransparentState
        {
            public string rc;
            public string state;
        }
        public static STransparentState replyTransparentState;

        public struct STransparentCommand
        {
            public string answer;
            public int sizeAnswer;
        }
        public static STransparentCommand replyTransparentCommand;

        #endregion

        #region TraceInit Non funziona
        [DllImport("CMTrace.dll", CallingConvention = CallingConvention.Winapi, CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true, EntryPoint = "CMTrace_Init")]
        public static extern short TraceInit(ELayer layer);
        #endregion

        #region CMTrace_Trace Non funziona
        [DllImport("CMTrace.dll", CallingConvention = CallingConvention.Winapi, CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true, EntryPoint = "CMTrace_Trace")]
        public static extern short TraceTrace(short hTrace, int dwTraceErrorLevel, ref string fmt);
        #endregion

        #region TRACE
        [DllImport("CMTrace.dll", CallingConvention = CallingConvention.Winapi, CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = false, EntryPoint = "CMTrace_SetTraceDirective")]
        public static extern short CMTrace_SetTraceDirective(STraceDirective traceDirective);

        [DllImport("CMTrace.dll", CallingConvention = CallingConvention.Winapi, CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = false, EntryPoint = "CMTrace_Init")]
        public static extern short CMTrace_Init(ELayer Layer);

        [DllImport("CMTrace.dll", CallingConvention = CallingConvention.Winapi, CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = false, EntryPoint = "CMTrace_Exit")]
        public static extern short CMTrace_Exit(ELayer Layer);
        [DllImport("CMTrace.dll", CallingConvention = CallingConvention.Winapi, CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = false, EntryPoint = "CMTrace_SetTraceDirectiveExt")]
        //[DllImport("CMTrace.dll")]
        //public static extern short CMTrace_SetTraceDirective(STraceDirective traceDirective);
        public static extern short CMTrace_SetTraceDirectiveExt(STraceDirective traceDirective, ref UInt32 lastError);
        //public static extern short CMTrace_SetTraceDirective_VB(ref STraceDirective traceDirective);

        public enum ELayer : Int32 { CMLINK, CMCOMMAND, SIBCASH, APPLICATION }
        public enum ILayer : Int16 { CMLINK, CMCOMMAND, SIBCASH, APPLICATION }
        public enum ETcrType : Int32 { CM_DIMENSION, CM_DATE }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public struct SComScope
        {
            public IntPtr hWin;
        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public struct STrComponent
        {
            public short level;
        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public struct STraceDirective
        {
            public ELayer infoLayer;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = MAX_PATH)]
            public string tracePathName;
            public SComScope lpComScope;
            public ETcrType trcType;
            public short trcSize;
            public short trcNum;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
            public STrComponent[] infoComp;

        }

        public static STraceDirective TraceDirective = new STraceDirective();
        public static STrComponent[] infocomp = new STrComponent[4];
        #endregion

        #region CONNECT - DISCONNECT

        //CMCommand_Connect
        [DllImport("CMCommand.dll", CallingConvention = CallingConvention.Winapi, CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true, EntryPoint = "CMCommand_Connect")]
        public static extern short CMConnect(ref short hCon, ref ConnectionParam ConnParam);

        //CMCommand_Disconnect
        [DllImport("CMCommand.dll", CallingConvention = CallingConvention.Winapi, CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true, EntryPoint = "CMCommand_Disconnect")]
        public static extern short CMDisconnect(short hCon);
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public struct ConnectionParam
        {
            public byte ConnectionMode;
            public RsConf pRsConf;
            public TcpIpPar TcpIpConf;
        };

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public struct RsConf
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 10)]
            public string device;
            public Int32 baudrate;
            public Int32 parity;
            public Int32 stop;
            public Int32 car;
            public bool dtr;
        };
        //public RsConf RSCONFIGURATION;

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public struct TcpIpPar
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 20)]
            public string clientIpAddr;
            public int portNumber;
        };
        public int CMConnect(ref ConnectionParam param)
        {
            return CMConnect(ref hCon, ref param);
        }

        public int CMDisconnect()
        {
            return CMDisconnect(hCon);
        }

        #endregion


        #region SINGLE COMMAND
        //CMCommand_Execute -  = ''
        [DllImport("CMCommand.dll", CallingConvention = CallingConvention.Winapi, CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true, EntryPoint = "CMCommand_Execute")]
        public static extern short CMSingleCommand(int hCon, byte idCommand, ref ISingle lpCmd, ref OSingle lpReply, int timeOut);
        public static ISingle InputSingle = new ISingle();
        public static OSingle OutputSingle = new OSingle();
        public struct ISingle
        {
            //[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 1700)]
            public string command;
            public int size;
        }
        public struct OSingle
        {
            //[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 1700)]
            public string answer;
            public int size;
        }
        public string CMSingleCommand(string command, int posReply = 3, int timeOut = 5000)
        {
            n++;
            if (n == 10)
            {
                n = 1;
            }
            int result;
            string reply;
            InputSingle.command = command;
            InputSingle.size = InputSingle.command.Length;
            OutputSingle.answer = new string(' ', 1700);
            OutputSingle.size = OutputSingle.answer.Length;
            commandSent = command;

            CommandSent(commandSent, null);
            result = CMSingleCommand(hCon, SINGLECMD, ref InputSingle, ref OutputSingle, timeOut);
            commandAns = OutputSingle.answer;
            CommandReceived(commandAns, null);
            commandSingleAns = commandAns.Split(',');
            try
            {
                reply = commandSingleAns[posReply];
                return reply;
            }
            catch (System.IndexOutOfRangeException)
            {
                return result.ToString();
            }

        }
        #endregion


        /// <summary>
        /// The Open command is used to establish a link between the Host and the machine
        /// </summary>
        /// <param name="side">Operator side (L/R)</param>
        /// <param name="code">Password relative to the operator selected</param>
        /// <returns></returns>
        public string CMOpen(string side = "L", string code = "123456")
        {
            string rc = CMSingleCommand("O," + n + "," + side + "," + code, 3, 5000);
            replyOpen = new SReplyOpen();
            replyOpen.rc = commandSingleAns[3];
            replyOpen.side = commandSingleAns[2];
            return rc;
        }

        /// <summary>
        /// Information regarding the cassettes enabled to the side specified (amount of notes contained in every cassette)
        /// </summary>
        /// <param name="side">Operator side (L/R)</param>
        /// <returns></returns>
        public string CMGetCashData(string side = "L")
        {
            string rc = CMSingleCommand("G," + n + "," + side, 3, 5000);

            int dim = ((commandSingleAns.Length) - 4) / 5;
            replyGetCashData = new SGetCashData();
            SCassetteInfo[] cassetteInfo = new SCassetteInfo[dim];
            replyGetCashData.side = commandSingleAns[2];
            replyGetCashData.rc = commandSingleAns[3];
            for (int i = 0; i < dim; i++)
            {
                cassetteInfo[i].noteId = commandSingleAns[i * 5 + 4];
                cassetteInfo[i].bnNumber = commandSingleAns[i * 5 + 5];
                cassetteInfo[i].bnFreeCap = commandSingleAns[i * 5 + 6];
                cassetteInfo[i].name = commandSingleAns[i * 5 + 7];
                cassetteInfo[i].enabled = commandSingleAns[i * 5 + 8];
            }
            replyGetCashData.CassetteInfo = cassetteInfo;
            replyGetCashData.numCasProt = dim.ToString();
            return rc;
        }

        /// <summary>
        /// Standard deposit in SAFE isong nation activated or nation specified in denomination
        /// </summary>
        /// <param name="side">Operator side (L/R)</param>
        /// <returns></returns>
        public string CMDeposit(string side = "L")
        {
            string rc = CMSingleCommand("D," + n + "," + side + ",0,0000", 3, 60000);
            int dim = ((commandSingleAns.Length) - 7) / 2;
            replyDeposit = new SDeposit();
            SNotesInfo[] notesInfo = new SNotesInfo[dim];
            replyDeposit.side = commandSingleAns[2];
            replyDeposit.rc = commandSingleAns[3];
            replyDeposit.bnTotal = Convert.ToInt16(commandSingleAns[4]);
            replyDeposit.bnOutput = Convert.ToInt16(commandSingleAns[5]);
            replyDeposit.bnRej = Convert.ToInt16(commandSingleAns[6]);
            for (int i = 0; i < dim; i++)
            {
                notesInfo[i].denomination = commandSingleAns[i * 2 + 7];
                notesInfo[i].bnNum = Convert.ToInt16(commandSingleAns[i * 2 + 8]);
            }
            replyDeposit.depositInfo = notesInfo;

            return rc;
        }

        /// <summary>
        /// Standard countig note. The notes are all directed to the output(FIT/UNFIT) and reject(suspect, not recognized)
        /// </summary>
        /// <param name="side">Operator side (L/R)</param>
        /// <returns></returns>
        public string CMCounting(string side = "L")
        {
            string rc = CMSingleCommand("D," + n + "," + side + ",3,0000", 3, 60000);
            int dim = ((commandSingleAns.Length) - 7) / 2;
            replyCounting = new SCounting();
            SNotesInfo[] notesInfo = new SNotesInfo[dim];
            replyCounting.side = commandSingleAns[2];
            replyCounting.rc = commandSingleAns[3];
            replyCounting.bnAccepted = Convert.ToInt16(commandSingleAns[4]);
            replyCounting.bnRej = Convert.ToInt16(commandSingleAns[5]);
            replyCounting.bnSuspect = Convert.ToInt16(commandSingleAns[6]);
            for (int i = 0; i < dim; i++)
            {
                notesInfo[i].denomination = commandSingleAns[i * 2 + 7];
                notesInfo[i].bnNum = Convert.ToInt16(commandSingleAns[i * 2 + 8]);
            }
            replyCounting.depositInfo = notesInfo;

            return rc;
        }

        /// <summary>
        /// FIT counting note. The notes are all directed to the output (FIT) and reject(all others)
        /// </summary>
        /// <param name="side">Operator side (L/R)</param>
        /// <returns></returns>
        public string CMFitCounting(string side = "L")
        {
            string rc = CMSingleCommand("D," + n + "," + side + ",10,0000", 3, 60000);
            int dim = ((commandSingleAns.Length) - 7) / 2;
            replyCounting = new SCounting();
            SNotesInfo[] notesInfo = new SNotesInfo[dim];
            replyCounting.side = commandSingleAns[2];
            replyCounting.rc = commandSingleAns[3];
            replyCounting.bnAccepted = Convert.ToInt16(commandSingleAns[4]);
            replyCounting.bnRej = Convert.ToInt16(commandSingleAns[5]);
            replyCounting.bnSuspect = Convert.ToInt16(commandSingleAns[6]);
            for (int i = 0; i < dim; i++)
            {
                notesInfo[i].denomination = commandSingleAns[i * 2 + 7];
                notesInfo[i].bnNum = Convert.ToInt16(commandSingleAns[i * 2 + 8]);
            }
            replyCounting.depositInfo = notesInfo;

            return rc;
        }

        /// <summary>
        /// With this command it is possible to cancel the deposits done until that moment or to accept them or to verify the amount of undo deposit
        /// </summary>
        /// <param name="side">Operator side</param>
        /// <param name="mode">0:UNDO DEPOSIT, 1:ACCEPT DEPOSIT, 2:VIEW LIST DEPOSIT</param>
        /// <returns></returns>
        public string CMUndo(string side = "L", string mode = "0")
        {
            string rc = CMSingleCommand("U," + n + "," + side + "," + mode, 3, 112000);
            int dim = ((commandSingleAns.Length) - 4) / 2;
            replyUndo = new SUndo();
            SNotesInfo[] notesInfo = new SNotesInfo[dim];
            replyUndo.side = commandSingleAns[2];
            replyUndo.rc = commandSingleAns[3];
            for (int i = 0; i < dim; i++)
            {
                notesInfo[i].denomination = commandSingleAns[i * 2 + 4];
                notesInfo[i].bnNum = Convert.ToInt16(commandSingleAns[i * 2 + 5]);
            }
            replyUndo.undoInfo = notesInfo;
            return rc;
        }

        /// <summary>
        /// This command delivers to the operator the requested number of notes for each specific denomination.
        /// </summary>
        /// <param name="noteInfo">stuct(SNotesInfo) of requested notes for each specific denomination</param>
        /// <param name="side">Operator side</param>
        /// <returns></returns>
        public string CMWithdrawal(SNotesInfo[] noteInfo = null, string side = "L")
        {
            string request = "";
            for (int i = 0; i < noteInfo.Length; i++)
            {
                request += "," + noteInfo[i].denomination + "," + noteInfo[i].bnNum;
            }
            string rc = CMSingleCommand("W," + n + "," + side + request, 3, 112000);
            int dim = ((commandSingleAns.Length) - 4) / 2;
            replyWithdrawal = new SWithdrawal();
            SNotesInfo[] notesInfo = new SNotesInfo[dim];
            replyWithdrawal.side = commandSingleAns[2];
            replyWithdrawal.rc = commandSingleAns[3];
            for (int i = 0; i < dim; i++)
            {
                notesInfo[i].denomination = commandSingleAns[i * 2 + 4];
                notesInfo[i].bnNum = Convert.ToInt16(commandSingleAns[i * 2 + 5]);
            }
            replyWithdrawal.noteInfo = notesInfo;
            return rc;
        }

        /// <summary>
        /// This command delivers to the selected CD80 cassettes the requested number of notes for each specific denomination
        /// </summary>
        /// <param name="side">Operator side</param>
        /// <param name="destination">CD80 cassette destination</param>
        /// <param name="noteInfo">stuct(SNotesInfo) of requested notes for each specific denomination</param>
        /// <returns></returns>
        public string CMTransfer(string side = "L", string destination = "A", SNotesInfo[] noteInfo = null)
        {
            string request = "";
            for (int i = 0; i < noteInfo.Length; i++)
            {
                request += "," + noteInfo[i].denomination + "," + noteInfo[i].bnNum;
            }

            string rc = CMSingleCommand("w," + n + "," + side + "," + destination + request, 3, 112000);
            int dim = ((commandSingleAns.Length) - 4) / 3;
            replyTranfer = new STransfer();
            STransferInfo[] transferInfo = new STransferInfo[dim];
            replyTranfer.side = commandSingleAns[2];
            replyTranfer.rc = commandSingleAns[3];
            for (int i = 0; i < dim; i++)
            {
                transferInfo[i].noteId = commandSingleAns[i * 3 + 4];
                transferInfo[i].bnNum = Convert.ToInt16(commandSingleAns[i * 3 + 5]);
                transferInfo[i].bnDep = Convert.ToInt16(commandSingleAns[i * 3 + 6]);
            }
            return rc;
        }

        /// <summary>
        /// This command will close the operator work session
        /// </summary>
        /// <param name="side">Operator side</param>
        /// <returns></returns>
        public string CMClose(string side = "L")
        {
            string rc = CMSingleCommand("C," + n + "," + side, 3, 5000);
            return rc;
        }

        /// <summary>
        /// This command give a detailed information regarding the status of every modules.
        /// </summary>
        /// <returns></returns>
        public string CMExtendedStatus()
        {
            string rc = CMSingleCommand("E," + n, 2, 5000);
            replyExtendedStatus = new SExtendedStatus();
            replyExtendedStatus.rc = commandSingleAns[2];
            replyExtendedStatus.feederStatus = commandSingleAns[3].Substring(1);
            replyExtendedStatus.controllerStatus = commandSingleAns[4].Substring(1);
            replyExtendedStatus.readerStatus = commandSingleAns[5].Substring(1);
            replyExtendedStatus.safeStatus = commandSingleAns[6].Substring(1);
            for (int i = 7; i < commandSingleAns.Length; i++)
            {
                switch (commandSingleAns[i].Substring(0, 1))
                {
                    case "A":
                        replyExtendedStatus.cassetteAStatus = commandSingleAns[i].Substring(1);
                        break;
                    case "B":
                        replyExtendedStatus.cassetteBStatus = commandSingleAns[i].Substring(1);
                        break;
                    case "C":
                        replyExtendedStatus.cassetteCStatus = commandSingleAns[i].Substring(1);
                        break;
                    case "D":
                        replyExtendedStatus.cassetteDStatus = commandSingleAns[i].Substring(1);
                        break;
                    case "E":
                        replyExtendedStatus.cassetteEStatus = commandSingleAns[i].Substring(1);
                        break;
                    case "F":
                        replyExtendedStatus.cassetteFStatus = commandSingleAns[i].Substring(1);
                        break;
                    case "G":
                        replyExtendedStatus.cassetteGStatus = commandSingleAns[i].Substring(1);
                        break;
                    case "H":
                        replyExtendedStatus.cassetteHStatus = commandSingleAns[i].Substring(1);
                        break;
                    case "I":
                        replyExtendedStatus.cassetteIStatus = commandSingleAns[i].Substring(1);
                        break;
                    case "J":
                        replyExtendedStatus.cassetteJStatus = commandSingleAns[i].Substring(1);
                        break;
                    case "K":
                        replyExtendedStatus.cassetteKStatus = commandSingleAns[i].Substring(1);
                        break;
                    case "L":
                        replyExtendedStatus.cassetteLStatus = commandSingleAns[i].Substring(1);
                        break;
                    case "q":
                        replyExtendedStatus.escrowQStatus = commandSingleAns[i].Substring(1);
                        break;
                    case "a":
                        replyExtendedStatus.depositAStatus = commandSingleAns[i].Substring(1);
                        break;
                    case "r":
                        replyExtendedStatus.escrowRStatus = commandSingleAns[i].Substring(1);
                        break;
                    case "b":
                        replyExtendedStatus.depositBStatus = commandSingleAns[i].Substring(1);
                        break;
                }
            }
            return rc;
        }

        /// <summary>
        /// This command give the firmware release and version of each single module
        /// </summary>
        /// <param name="module">Module address or name</param>
        /// <returns></returns>
        public string CMVersion(string module)
        {
            string rc;
            if (module.Length > 1)
            {
                switch (module.ToUpper())
                {
                    case "CONTROLLER":
                        module = "0";
                        break;
                    case "FEEDER":
                        module = "1";
                        break;
                    case "RTC":
                    case "REAL TIME":
                    case "REAL TIME CONTROLLER":
                        module = "2";
                        break;
                    case "READER":
                    case "IDENTIFIER":
                        module = "3";
                        break;
                    case "SAFE":
                        module = "4";
                        break;
                    case "5":
                        module = "5";
                        break;
                    case "OP SYSTEM":
                        module = "6";
                        break;
                    case "CASSETTOA":
                        module = "A";
                        break;
                    case "CASSETTOB":
                        module = "B";
                        break;
                    case "CASSETTOC":
                        module = "C";
                        break;
                    case "CASSETTOD":
                        module = "D";
                        break;
                    case "CASSETTOE":
                        module = "E";
                        break;
                    case "CASSETTOF":
                        module = "F";
                        break;
                    case "CASSETTOG":
                        module = "G";
                        break;
                    case "CASSETTOH":
                        module = "H";
                        break;
                    case "CASSETTOI":
                        module = "I";
                        break;
                    case "CASSETTOJ":
                        module = "J";
                        break;
                    case "CASSETTOK":
                        module = "K";
                        break;
                    case "CASSETTOL":
                        module = "L";
                        break;
                    case "DEPOSITA":
                        module = "a";
                        break;
                    case "DEPOSITB":
                        module = "b";
                        break;
                    default:
                        rc = "KO";
                        break;
                }
            }
            rc = CMSingleCommand("V," + n + "," + module, 2, 5000);
            replyVersion = new SVersion();
            replyVersion.rc = commandSingleAns[2];
            switch (module)
            {
                case "0":
                    replyVersion.controller.ver = commandSingleAns[3];
                    replyVersion.controller.rel = commandSingleAns[4];
                    break;
                case "1":
                    replyVersion.feeder.ver = commandSingleAns[3];
                    replyVersion.feeder.rel = commandSingleAns[4];
                    break;
                case "2":
                    replyVersion.realTime.ver = commandSingleAns[3];
                    replyVersion.realTime.rel = commandSingleAns[4];
                    break;
                case "3":
                    replyVersion.reader.ver = commandSingleAns[3];
                    replyVersion.reader.rel = commandSingleAns[4];
                    break;
                case "4":
                    replyVersion.safe.ver = commandSingleAns[3];
                    replyVersion.safe.rel = commandSingleAns[4];
                    break;
                case "5":
                    replyVersion.FPGA.ver = commandSingleAns[3];
                    replyVersion.FPGA.rel = commandSingleAns[4];
                    break;
                case "6":
                    replyVersion.opSystem.ver = commandSingleAns[3];
                    replyVersion.opSystem.rel = commandSingleAns[4];
                    break;
                case "A":
                    replyVersion.cassetteA.ver = commandSingleAns[3];
                    replyVersion.cassetteA.rel = commandSingleAns[4];
                    break;
                case "B":
                    replyVersion.cassetteB.ver = commandSingleAns[3];
                    replyVersion.cassetteB.rel = commandSingleAns[4];
                    break;
                case "C":
                    replyVersion.cassetteC.ver = commandSingleAns[3];
                    replyVersion.cassetteC.rel = commandSingleAns[4];
                    break;
                case "D":
                    replyVersion.cassetteD.ver = commandSingleAns[3];
                    replyVersion.cassetteD.rel = commandSingleAns[4];
                    break;
                case "E":
                    replyVersion.cassetteE.ver = commandSingleAns[3];
                    replyVersion.cassetteE.rel = commandSingleAns[4];
                    break;
                case "F":
                    replyVersion.cassetteF.ver = commandSingleAns[3];
                    replyVersion.cassetteF.rel = commandSingleAns[4];
                    break;
                case "G":
                    replyVersion.cassetteG.ver = commandSingleAns[3];
                    replyVersion.cassetteG.rel = commandSingleAns[4];
                    break;
                case "H":
                    replyVersion.cassetteH.ver = commandSingleAns[3];
                    replyVersion.cassetteH.rel = commandSingleAns[4];
                    break;
                case "I":
                    replyVersion.cassetteI.ver = commandSingleAns[3];
                    replyVersion.cassetteI.rel = commandSingleAns[4];
                    break;
                case "J":
                    replyVersion.cassetteJ.ver = commandSingleAns[3];
                    replyVersion.cassetteJ.rel = commandSingleAns[4];
                    break;
                case "K":
                    replyVersion.cassetteK.ver = commandSingleAns[3];
                    replyVersion.cassetteK.rel = commandSingleAns[4];
                    break;
                case "L":
                    replyVersion.cassetteL.ver = commandSingleAns[3];
                    replyVersion.cassetteL.rel = commandSingleAns[4];
                    break;
                case "a":
                    replyVersion.depositA.ver = commandSingleAns[3];
                    replyVersion.depositA.rel = commandSingleAns[4];
                    break;
                case "b":
                    replyVersion.depositB.ver = commandSingleAns[3];
                    replyVersion.depositB.rel = commandSingleAns[4];
                    break;
            }
            return rc;
        }

        /// <summary>
        /// This command initializes the cassette logical address according to the physical position
        /// </summary>
        /// <returns></returns>
        public string CMAssignCassette()
        {
            string rc = CMSingleCommand("A," + n, 2, 5000);
            replyAssign = new SAssign();
            replyAssign.rc = commandSingleAns[2];
            replyAssign.message = commandSingleAns[3];
            return rc;
        }





        /*    GET CONFIG 2.16 / SET CONFIG 2.17       pg 51/52         */





        /// <summary>
        /// This command will give the date and time info to the device
        /// </summary>
        /// <param name="mm">Month (1 - 12)</param>
        /// <param name="dd">Day (1 - 31)</param>
        /// <param name="hh">Hour (1 - 24)</param>
        /// <param name="pp">Minutes (0 - 59)</param>
        /// <returns></returns>
        public string CMSetDateTime(int mm = 0, int dd = 0, int hh = 0, int pp = 0)
        {
            string rc = "";
            if (mm == 0 && dd == 0)
            {
                mm = Convert.ToInt16(DateTime.Now.Month);
                dd = Convert.ToInt16(DateTime.Now.Day);
            }

            if (hh == 0 && pp == 0)
            {
                hh = Convert.ToInt16(DateTime.Now.Hour);
                pp = Convert.ToInt16(DateTime.Now.Minute);
            }

            if (mm < 1 | mm > 12 | dd < 1 | dd > 31)
            {
                rc = "WRONG DATE";
            }

            if (hh < 1 | hh > 24 | hh < 0 | hh > 59)
            {
                rc += " WRONG TIME";
            }

            if (rc == "")
            {
                rc = CMSingleCommand("F," + n + ",0," + mm + "," + dd + "," + hh + "," + pp, 3, 5000);
            }
            replySetDateTime = new SRcOnly();
            replySetDateTime.rc = commandSingleAns[3];
            return rc;
        }

        /// <summary>
        /// This command configure some functionality parameters on the CM
        /// </summary>
        /// <param name="config">Hex value for entire configuration (4 digit)</param>
        /// <param name="x0001">0/1 - View date & time on display</param>
        /// <param name="x0002">reserved</param>
        /// <param name="x0004">reserved</param>
        /// <param name="x0008">reserved</param>
        /// <param name="x0010">reserved</param>
        /// <param name="x0020">reserved</param>
        /// <param name="x0040">0/1 - Balanced cassette handling</param>
        /// <param name="x0080">0/1 - Alarm 1 handling</param>
        /// <param name="x0100">0/1 - Use delay class dispensing</param>
        /// <param name="x0200">reserved</param>
        /// <param name="x0400">0/1 - Data & time on display in AM/PM format</param>
        /// <param name="x0800">0/1 - Use UNFIT in SAFE</param>
        /// <param name="x1000">reserved</param>
        /// <param name="x2000">reserved</param>
        /// <param name="x4000">reserved</param>
        /// <param name="x8000">0/1 - Enable Journal Log</param>
        /// <returns></returns>
        public string CMFillConfig(string config = "", int x0001 = 0, int x0002 = 0, int x0004 = 0, int x0008 = 0, int x0010 = 0, int x0020 = 0, int x0040 = 0, int x0080 = 0, int x0100 = 0, int x0200 = 0, int x0400 = 0, int x0800 = 0, int x1000 = 0, int x2000 = 0, int x4000 = 0, int x8000 = 0)
        {

            if (config == "")
            {
                x0001 = x0001 == 1 ? 0x0001 : 0;
                x0002 = x0002 == 1 ? 0x0002 : 0;
                x0004 = x0004 == 1 ? 0x0004 : 0;
                x0008 = x0008 == 1 ? 0x0008 : 0;
                x0010 = x0010 == 1 ? 0x0010 : 0;
                x0020 = x0020 == 1 ? 0x0020 : 0;
                x0040 = x0040 == 1 ? 0x0040 : 0;
                x0080 = x0080 == 1 ? 0x0080 : 0;
                x0100 = x0100 == 1 ? 0x0100 : 0;
                x0200 = x0200 == 1 ? 0x0200 : 0;
                x0400 = x0400 == 1 ? 0x0400 : 0;
                x0800 = x0800 == 1 ? 0x0800 : 0;
                x1000 = x1000 == 1 ? 0x1000 : 0;
                x2000 = x2000 == 1 ? 0x2000 : 0;
                x4000 = x4000 == 1 ? 0x4000 : 0;
                x8000 = x8000 == 1 ? 0x8000 : 0;

                config = (x0001 + x0002 + x0004 + x0008 + x0010 + x0020 + x0040 + x0080 + x0100 + x0200 + x0400 + x0800 + x1000 + x2000 + x4000 + x8000).ToString("X4");
            }

            replyFillCmConfig = new SRcOnly();
            replyFillCmConfig.rc = CMSingleCommand("F," + n + ",7," + config, 3, 5000);
            return replyFillCmConfig.rc;
        }

        /// <summary>
        /// With this command it is possible to configure some functionality parameters on the CM and to see yours effects is necessary to switch off and on the machine
        /// </summary>
        /// <param name="config">Hex value for entire configuration (4 digit)</param>
        /// <param name="x0001">Special clean without control</param>
        /// <param name="x0002">reserved</param>
        /// <param name="x0004">reserved</param>
        /// <param name="x0008">Special alarm mode</param>
        /// <param name="x0010">Enable Simplified protocol</param>
        /// <param name="x0020">Execute Close commando also in Jam</param>
        /// <param name="x0040">reserved</param>
        /// <param name="x0080">reserved</param>
        /// <param name="x0100">reserved</param>
        /// <param name="x0200">reserved</param>
        /// <param name="x0400">reserved</param>
        /// <param name="x0800">reserved</param>
        /// <param name="x1000">reserved</param>
        /// <param name="x2000">reserved</param>
        /// <param name="x4000">reserved</param>
        /// <param name="x8000">reserved</param>
        /// <returns></returns>
        public string CMFillOptionConfig(string config = "", int x0001 = 0, int x0002 = 0, int x0004 = 0, int x0008 = 0, int x0010 = 0, int x0020 = 0, int x0040 = 0, int x0080 = 0, int x0100 = 0, int x0200 = 0, int x0400 = 0, int x0800 = 0, int x1000 = 0, int x2000 = 0, int x4000 = 0, int x8000 = 0)
        {

            if (config == "")
            {
                x0001 = x0001 == 1 ? 0x0001 : 0;
                x0002 = x0002 == 1 ? 0x0002 : 0;
                x0004 = x0004 == 1 ? 0x0004 : 0;
                x0008 = x0008 == 1 ? 0x0008 : 0;
                x0010 = x0010 == 1 ? 0x0010 : 0;
                x0020 = x0020 == 1 ? 0x0020 : 0;
                x0040 = x0040 == 1 ? 0x0040 : 0;
                x0080 = x0080 == 1 ? 0x0080 : 0;
                x0100 = x0100 == 1 ? 0x0100 : 0;
                x0200 = x0200 == 1 ? 0x0200 : 0;
                x0400 = x0400 == 1 ? 0x0400 : 0;
                x0800 = x0800 == 1 ? 0x0800 : 0;
                x1000 = x1000 == 1 ? 0x1000 : 0;
                x2000 = x2000 == 1 ? 0x2000 : 0;
                x4000 = x4000 == 1 ? 0x4000 : 0;
                x8000 = x8000 == 1 ? 0x8000 : 0;

                config = (x0001 + x0002 + x0004 + x0008 + x0010 + x0020 + x0040 + x0080 + x0100 + x0200 + x0400 + x0800 + x1000 + x2000 + x4000 + x8000).ToString("X4");
            }

            replyFillCmOptionConfig = new SRcOnly();
            replyFillCmOptionConfig.rc = CMSingleCommand("F," + n + ",8," + config, 3, 5000);
            return replyFillCmOptionConfig.rc;
        }

        /// <summary>
        /// With this command it is possible to configure some functionality parameters on the CM and to see yours effects is necessary to switch off and on the machine.
        /// </summary>
        /// <param name="config">Hex value for entire configuration (4 digit)</param>
        /// <param name="x0001">reserved</param>
        /// <param name="x0002">reserved</param>
        /// <param name="x0004">reserved</param>
        /// <param name="x0008">Impac 0mm on cassette CR37</param>
        /// <param name="x0010">reserved</param>
        /// <param name="x0020">reserved</param>
        /// <param name="x0040">reserved</param>
        /// <param name="x0080">Don't remove cass jam when special clean without control enabled</param>
        /// <param name="x0100">reserved</param>
        /// <param name="x0200">reserved</param>
        /// <param name="x0400">reserved</param>
        /// <param name="x0800">Timeout connect/disconnect LAN</param>
        /// <param name="x1000">Enable time control dispense amount (TCDA)</param>
        /// <param name="x2000">reserved</param>
        /// <param name="x4000">Enable booking open by external button</param>
        /// <param name="x8000">reserved</param>
        /// <returns></returns>
        public string CMFillOptionOneConfig(string config = "", int x0001 = 0, int x0002 = 0, int x0004 = 0,
            int x0008 = 0, int x0010 = 0, int x0020 = 0, int x0040 = 0, int x0080 = 0, int x0100 = 0, int x0200 = 0,
            int x0400 = 0, int x0800 = 0, int x1000 = 0, int x2000 = 0, int x4000 = 0, int x8000 = 0)
        {

            if (config == "")
            {
                x0001 = x0001 == 1 ? 0x0001 : 0;
                x0002 = x0002 == 1 ? 0x0002 : 0;
                x0004 = x0004 == 1 ? 0x0004 : 0;
                x0008 = x0008 == 1 ? 0x0008 : 0;
                x0010 = x0010 == 1 ? 0x0010 : 0;
                x0020 = x0020 == 1 ? 0x0020 : 0;
                x0040 = x0040 == 1 ? 0x0040 : 0;
                x0080 = x0080 == 1 ? 0x0080 : 0;
                x0100 = x0100 == 1 ? 0x0100 : 0;
                x0200 = x0200 == 1 ? 0x0200 : 0;
                x0400 = x0400 == 1 ? 0x0400 : 0;
                x0800 = x0800 == 1 ? 0x0800 : 0;
                x1000 = x1000 == 1 ? 0x1000 : 0;
                x2000 = x2000 == 1 ? 0x2000 : 0;
                x4000 = x4000 == 1 ? 0x4000 : 0;
                x8000 = x8000 == 1 ? 0x8000 : 0;

                config = (x0001 + x0002 + x0004 + x0008 + x0010 + x0020 + x0040 + x0080 + x0100 + x0200 + x0400 + x0800 +
                    x1000 + x2000 + x4000 + x8000).ToString("X4");
            }

            replyFillCmOptionOneConfig = new SRcOnly();
            replyFillCmOptionOneConfig.rc = CMSingleCommand("F," + n + ",9," + config, 3, 5000);
            return replyFillCmOptionOneConfig.rc;
        }

        /// <summary>
        /// With this command it is possible to configure some functionality parameters on the CM and to see yours effects is necessary to switch off and on the machine
        /// </summary>
        /// <param name="config">Hex value for entire configuration (8 digit)</param>
        /// <param name="x00000001">Enable 40mm Safe</param>
        /// <param name="x00000002">BVU synchronous handling</param>
        /// <param name="x00000004">Banknotes orientation in safe</param>
        /// <param name="x00000008">Special clean using display</param>
        /// <param name="x00000010">Handling cat 2 box</param>
        /// <param name="x00000020">reserved</param>
        /// <param name="x00000040">EVO led handling</param>
        /// <param name="x00000080">Separate Cat1 and Cat3</param>
        /// <param name="x00000100">reserved</param>
        /// <param name="x00000200">Feed banknotes with input bin free</param>
        /// <param name="x00000400">Different center AU banknotes</param>
        /// <param name="x00000800">reserved</param>
        /// <param name="x00001000">Block recovery after power off in cash movements</param>
        /// <param name="x00002000">Shutter handling</param>
        /// <param name="x00004000">reserved</param>
        /// <param name="x00008000">Disable message on display</param>
        /// <param name="x00010000">Alarm2 output handling</param>
        /// <returns></returns>
        public string CMFillOptionTwoConfig(string config = "", int x00000001 = 0, int x00000002 = 0, int x00000004 = 0,
            int x00000008 = 0, int x00000010 = 0, int x00000020 = 0, int x00000040 = 0, int x00000080 = 0, int x00000100 = 0,
            int x00000200 = 0, int x00000400 = 0, int x00000800 = 0, int x00001000 = 0, int x00002000 = 0, int x00004000 = 0,
            int x00008000 = 0, int x00010000 = 0)
        {

            if (config == "")
            {
                x00000001 = x00000001 == 1 ? 0x00000001 : 0;
                x00000002 = x00000002 == 1 ? 0x00000002 : 0;
                x00000004 = x00000004 == 1 ? 0x00000004 : 0;
                x00000008 = x00000008 == 1 ? 0x00000008 : 0;
                x00000010 = x00000010 == 1 ? 0x00000010 : 0;
                x00000020 = x00000020 == 1 ? 0x00000020 : 0;
                x00000040 = x00000040 == 1 ? 0x00000040 : 0;
                x00000080 = x00000080 == 1 ? 0x00000080 : 0;
                x00000100 = x00000100 == 1 ? 0x00000100 : 0;
                x00000200 = x00000200 == 1 ? 0x00000200 : 0;
                x00000400 = x00000400 == 1 ? 0x00000400 : 0;
                x00000800 = x00000800 == 1 ? 0x00000800 : 0;
                x00001000 = x00001000 == 1 ? 0x00001000 : 0;
                x00002000 = x00002000 == 1 ? 0x00002000 : 0;
                x00004000 = x00004000 == 1 ? 0x00004000 : 0;
                x00008000 = x00008000 == 1 ? 0x00008000 : 0;
                x00010000 = x00010000 == 1 ? 0x00010000 : 0;

                config = (x00000001 + x00000002 + x00000004 + x00000008 + x00000010 + x00000020 +
                    x00000040 + x00000080 + x00000100 + x00000200 + x00000400 + x00000800 + x00001000 +
                    x00002000 + x00004000 + x00008000 + x00010000).ToString("X8");
            }

            replyFillCmOptionTwoConfig = new SRcOnly();
            replyFillCmOptionTwoConfig.rc = CMSingleCommand("F," + n + ",23," + config, 3, 5000);
            return replyFillCmOptionTwoConfig.rc;
        }

        /// <summary>
        /// With this command it is possible to set the identification code of the device.
        /// </summary>
        /// <param name="identification">Unit identification (CM20, CM18, CM18T, CM18E, CM20E, CM18B)</param>
        /// <returns></returns>
        public string CMFillUnitIdentication(string identification = "CM18T")
        {
            replyFillCmUnitIdentification = new SRcOnly();
            replyFillCmUnitIdentification.rc = CMSingleCommand("F," + n + ",10," + identification, 3, 5000);
            return replyFillCmUnitIdentification.rc;
        }

        /// <summary>
        /// With this command it is possible to set the number of the cassettes of the device.
        /// </summary>
        /// <param name="numCas">Number of cassettes installed (4, 6, 8, 10, 12)</param>
        /// <returns></returns>
        public string CMFillCassetteNumber(int numCas = 8)
        {
            replyFillCmCassetteNumber = new SRcOnly();
            replyFillCmCassetteNumber.rc = CMSingleCommand("F," + n + ",11," + numCas, 3, 5000);
            return replyFillCmCassetteNumber.rc;
        }

        /// <summary>
        /// With this command it is possible to set the maximum number of banknotes in output slot during the operation of withdrawal(the default is 200)
        /// </summary>
        /// <param name="numNotes">Number of notes</param>
        /// <returns></returns>
        public string CMFillWithdrawalBundleSize(int numNotes = 200)
        {
            replyFillWithdrawallBundleSize = new SRcOnly();
            replyFillWithdrawallBundleSize.rc = CMSingleCommand("F," + n + ",15," + numNotes, 3, 5000);
            return replyFillWithdrawallBundleSize.rc;
        }

        /// <summary>
        /// With this command it is possible to set the number of banknotes in output slot during a counting operation(the default is 200)
        /// </summary>
        /// <param name="numNotes">>Number of notes</param>
        /// <returns></returns>
        public string CMFillCountingBundleSize(int numNotes = 200)
        {
            replyFillCountingBundleSize = new SRcOnly();
            replyFillCountingBundleSize.rc = CMSingleCommand("F," + n + ",18," + numNotes, 3, 5000);
            return replyFillCountingBundleSize.rc;
        }

        /// <summary>
        /// With this command it is possible to set the number of the cassettes used by communication protocol. It’s possible to configure this parameter only with CM18T because CM18/CM18E use fixed cassettes number to 8 whereas CM20 / CM20E use fixed cassettes number to 10.
        /// </summary>
        /// <param name="numProtCas">Number of cassette in protocol</param>
        /// <returns></returns>
        public string CMFillProtocolCassetteNumber(int numProtCas = 8)
        {
            replyFillCmProtCassNumber = new SRcOnly();
            replyFillCmProtCassNumber.rc = CMSingleCommand("F," + n + ",19," + numProtCas, 3, 5000);
            return replyFillCmProtCassNumber.rc;
        }

        /// <summary>
        /// With this command it is possible to set the number of CD80 cassettes
        /// </summary>
        /// <param name="numCD80">Number of CD80 cassette installed (0, 2, 4)</param>
        /// <returns></returns>
        public string CMFillCD80CassetteNumber(int numCD80 = 0)
        {
            replyFillCD80CassNumber = new SRcOnly();
            replyFillCD80CassNumber.rc = CMSingleCommand("F," + n + ",30," + numCD80, 3, 5000);
            return replyFillCD80CassNumber.rc;
        }


        /// <summary>
        /// Check the status of all the unit covers/door/input/output slots, such as Safe door, top cover, input slot, etc.
        /// </summary>
        /// <returns></returns>
        public string CMUnitCoverTest()
        {
            replyUnitCoverTest = new SUnitCoverTest();
            replyUnitCoverTest.rc = CMSingleCommand("T," + n + ",0,0", 4, 5000);
            replyUnitCoverTest.safeDoor = commandSingleAns[5];
            replyUnitCoverTest.cover = commandSingleAns[7];
            replyUnitCoverTest.feeder = commandSingleAns[8];
            replyUnitCoverTest.inputSlot = commandSingleAns[9];
            replyUnitCoverTest.rejectSlot = commandSingleAns[10];
            replyUnitCoverTest.leftSlot = commandSingleAns[11];
            replyUnitCoverTest.rightSlot = commandSingleAns[12];

            return replyUnitCoverTest.rc;
        }

        /// <summary>
        /// This command is used to ENTER in transparent mode
        /// </summary>
        /// <returns></returns>
        public string CMTransparentIn()
        {
            replyTransparentState = new STransparentState();
            replyTransparentState.rc = CMSingleCommand("X," + n, 2, 5000);
            if (replyTransparentState.rc == "101" | replyTransparentState.rc == "1")
            {
                replyTransparentState.state = "ON";
            }
            return replyTransparentState.rc;
        }

        /// <summary>
        /// This command is used to EXIT from transparent mode
        /// </summary>
        /// <returns></returns>
        public string CMTransparentOut()
        {
            replyTransparentState = new STransparentState();
            replyTransparentState.rc = CMSingleCommand("Y," + n, 2, 5000);
            if (replyTransparentState.rc == "101" | replyTransparentState.rc == "1")
            {
                replyTransparentState.state = "OFF";
            }
            return replyTransparentState.rc;
        }

        /// <summary>
        /// This command send a Transparent command. It works only if the transparent mode is active
        /// </summary>
        /// <param name="command"></param>
        /// <param name="timeOut"></param>
        /// <returns></returns>
        public string CMTransparentCommand(string command, int timeOut = 1000)
        {

            replyTransparentCommand = new STransparentCommand();

            if (replyTransparentState.state != "ON")
            {
                return "TRASP OFF";
            }
            int result;
            InputSingle.command = command;
            InputSingle.size = InputSingle.command.Length;
            OutputSingle.answer = new string(' ', 1700);
            OutputSingle.size = OutputSingle.answer.Length;
            commandSent = command;

            //ComScope.AddListBoxItem("SND: " + commandSent);
            result = CMSingleCommand(hCon, TRANSPARENT, ref InputSingle, ref OutputSingle, timeOut);
            commandAns = OutputSingle.answer;
            replyTransparentCommand.answer = commandAns;
            //ComScope.AddItem("RCV: " + commandAns);
            commandSingleAns = commandAns.Split(',');
            try
            {

                return replyTransparentCommand.answer;
            }
            catch (System.IndexOutOfRangeException)
            {
                return result.ToString();
            }
        }
    }
}
