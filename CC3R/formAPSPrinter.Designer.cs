﻿namespace CC3R
{
    partial class frmAPSPrinter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAPSPrinter));
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnCutFull = new System.Windows.Forms.Button();
            this.btnCutPartial = new System.Windows.Forms.Button();
            this.btnIdentity = new System.Windows.Forms.Button();
            this.btnStatus = new System.Windows.Forms.Button();
            this.grpCommands = new System.Windows.Forms.GroupBox();
            this.btnFeedPaper = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtAscii = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtHex = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtSend = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtToPrint = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.btnSelectFont = new System.Windows.Forms.Button();
            this.grpCommands.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnPrint
            // 
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.Location = new System.Drawing.Point(262, 19);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(120, 70);
            this.btnPrint.TabIndex = 11;
            this.btnPrint.Text = "Print";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnCutFull
            // 
            this.btnCutFull.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCutFull.Location = new System.Drawing.Point(134, 95);
            this.btnCutFull.Name = "btnCutFull";
            this.btnCutFull.Size = new System.Drawing.Size(120, 70);
            this.btnCutFull.TabIndex = 12;
            this.btnCutFull.Text = "Full Cut";
            this.btnCutFull.UseVisualStyleBackColor = true;
            this.btnCutFull.Click += new System.EventHandler(this.btnCutFull_Click);
            // 
            // btnCutPartial
            // 
            this.btnCutPartial.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCutPartial.Location = new System.Drawing.Point(6, 95);
            this.btnCutPartial.Name = "btnCutPartial";
            this.btnCutPartial.Size = new System.Drawing.Size(120, 70);
            this.btnCutPartial.TabIndex = 13;
            this.btnCutPartial.Text = "Partial Cut";
            this.btnCutPartial.UseVisualStyleBackColor = true;
            this.btnCutPartial.Click += new System.EventHandler(this.btnCutPartial_Click);
            // 
            // btnIdentity
            // 
            this.btnIdentity.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIdentity.Location = new System.Drawing.Point(6, 19);
            this.btnIdentity.Name = "btnIdentity";
            this.btnIdentity.Size = new System.Drawing.Size(120, 70);
            this.btnIdentity.TabIndex = 14;
            this.btnIdentity.Text = "Identity";
            this.btnIdentity.UseVisualStyleBackColor = true;
            this.btnIdentity.Click += new System.EventHandler(this.btnIdentity_Click);
            // 
            // btnStatus
            // 
            this.btnStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStatus.Location = new System.Drawing.Point(134, 19);
            this.btnStatus.Name = "btnStatus";
            this.btnStatus.Size = new System.Drawing.Size(120, 70);
            this.btnStatus.TabIndex = 15;
            this.btnStatus.Text = "Status";
            this.btnStatus.UseVisualStyleBackColor = true;
            this.btnStatus.Click += new System.EventHandler(this.btnStatus_Click);
            // 
            // grpCommands
            // 
            this.grpCommands.Controls.Add(this.button2);
            this.grpCommands.Controls.Add(this.button3);
            this.grpCommands.Controls.Add(this.btnSelectFont);
            this.grpCommands.Controls.Add(this.btnFeedPaper);
            this.grpCommands.Controls.Add(this.btnStatus);
            this.grpCommands.Controls.Add(this.btnPrint);
            this.grpCommands.Controls.Add(this.btnIdentity);
            this.grpCommands.Controls.Add(this.btnCutFull);
            this.grpCommands.Controls.Add(this.btnCutPartial);
            this.grpCommands.Location = new System.Drawing.Point(18, 6);
            this.grpCommands.Name = "grpCommands";
            this.grpCommands.Size = new System.Drawing.Size(392, 272);
            this.grpCommands.TabIndex = 16;
            this.grpCommands.TabStop = false;
            this.grpCommands.Text = "COMMANDS";
            // 
            // btnFeedPaper
            // 
            this.btnFeedPaper.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFeedPaper.Location = new System.Drawing.Point(262, 95);
            this.btnFeedPaper.Name = "btnFeedPaper";
            this.btnFeedPaper.Size = new System.Drawing.Size(120, 70);
            this.btnFeedPaper.TabIndex = 16;
            this.btnFeedPaper.Text = "Feed Paper";
            this.btnFeedPaper.UseVisualStyleBackColor = true;
            this.btnFeedPaper.Click += new System.EventHandler(this.btnFeedPaper_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtAscii);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.txtHex);
            this.groupBox2.Location = new System.Drawing.Point(416, 284);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(356, 168);
            this.groupBox2.TabIndex = 17;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "RECEIVED";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 90);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "ASCII";
            // 
            // txtAscii
            // 
            this.txtAscii.Location = new System.Drawing.Point(10, 106);
            this.txtAscii.Multiline = true;
            this.txtAscii.Name = "txtAscii";
            this.txtAscii.Size = new System.Drawing.Size(330, 54);
            this.txtAscii.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "HEX";
            // 
            // txtHex
            // 
            this.txtHex.Location = new System.Drawing.Point(10, 31);
            this.txtHex.Multiline = true;
            this.txtHex.Name = "txtHex";
            this.txtHex.Size = new System.Drawing.Size(330, 56);
            this.txtHex.TabIndex = 1;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtSend);
            this.groupBox3.Location = new System.Drawing.Point(18, 284);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(392, 168);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "SEND";
            // 
            // txtSend
            // 
            this.txtSend.Location = new System.Drawing.Point(10, 22);
            this.txtSend.Multiline = true;
            this.txtSend.Name = "txtSend";
            this.txtSend.Size = new System.Drawing.Size(374, 138);
            this.txtSend.TabIndex = 0;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtToPrint);
            this.groupBox4.Location = new System.Drawing.Point(416, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(356, 272);
            this.groupBox4.TabIndex = 18;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "TEXT TO PRINT";
            // 
            // txtToPrint
            // 
            this.txtToPrint.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtToPrint.Location = new System.Drawing.Point(10, 22);
            this.txtToPrint.Multiline = true;
            this.txtToPrint.Name = "txtToPrint";
            this.txtToPrint.Size = new System.Drawing.Size(330, 237);
            this.txtToPrint.TabIndex = 0;
            this.txtToPrint.Text = resources.GetString("txtToPrint.Text");
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(18, 458);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(754, 42);
            this.button1.TabIndex = 19;
            this.button1.Text = "Exit";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(262, 171);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(120, 70);
            this.button2.TabIndex = 19;
            this.button2.Text = "-";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(134, 171);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(120, 70);
            this.button3.TabIndex = 17;
            this.button3.Text = "-";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // btnSelectFont
            // 
            this.btnSelectFont.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelectFont.Location = new System.Drawing.Point(6, 171);
            this.btnSelectFont.Name = "btnSelectFont";
            this.btnSelectFont.Size = new System.Drawing.Size(120, 70);
            this.btnSelectFont.TabIndex = 18;
            this.btnSelectFont.Text = "Select Font 12x20";
            this.btnSelectFont.UseVisualStyleBackColor = true;
            this.btnSelectFont.Click += new System.EventHandler(this.btnSelectFont_Click);
            // 
            // frmAPSPrinter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 506);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.grpCommands);
            this.Name = "frmAPSPrinter";
            this.Text = "formAPSPrinter";
            this.Load += new System.EventHandler(this.frmAPSPrinter_Load);
            this.grpCommands.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnCutFull;
        private System.Windows.Forms.Button btnCutPartial;
        private System.Windows.Forms.Button btnIdentity;
        private System.Windows.Forms.Button btnStatus;
        private System.Windows.Forms.GroupBox grpCommands;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtAscii;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtHex;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtSend;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txtToPrint;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnFeedPaper;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btnSelectFont;
    }
}