﻿namespace CC3R
{
    partial class frmCTCoin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblConnection = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnDisconnect = new System.Windows.Forms.Button();
            this.btnConnect = new System.Windows.Forms.Button();
            this.btnDisplayCont = new System.Windows.Forms.Button();
            this.btnSingleCommand = new System.Windows.Forms.Button();
            this.txtSingleCommand = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSingleAnswer = new System.Windows.Forms.TextBox();
            this.txtDisplay = new System.Windows.Forms.TextBox();
            this.btnGetKeypadPress = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblConnection);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnDisconnect);
            this.groupBox1.Controls.Add(this.btnConnect);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(275, 146);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "CONNECTION";
            // 
            // lblConnection
            // 
            this.lblConnection.AutoSize = true;
            this.lblConnection.Location = new System.Drawing.Point(64, 123);
            this.lblConnection.Name = "lblConnection";
            this.lblConnection.Size = new System.Drawing.Size(78, 13);
            this.lblConnection.TabIndex = 3;
            this.lblConnection.Text = "Not connected";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 123);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Status:";
            // 
            // btnDisconnect
            // 
            this.btnDisconnect.Location = new System.Drawing.Point(138, 19);
            this.btnDisconnect.Name = "btnDisconnect";
            this.btnDisconnect.Size = new System.Drawing.Size(126, 95);
            this.btnDisconnect.TabIndex = 1;
            this.btnDisconnect.Text = "DISCONNECT";
            this.btnDisconnect.UseVisualStyleBackColor = true;
            this.btnDisconnect.Click += new System.EventHandler(this.btnDisconnect_Click);
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(6, 19);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(126, 95);
            this.btnConnect.TabIndex = 0;
            this.btnConnect.Text = "CONNECT";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // btnDisplayCont
            // 
            this.btnDisplayCont.Location = new System.Drawing.Point(12, 174);
            this.btnDisplayCont.Name = "btnDisplayCont";
            this.btnDisplayCont.Size = new System.Drawing.Size(126, 42);
            this.btnDisplayCont.TabIndex = 1;
            this.btnDisplayCont.Text = "DISPLAY CONTENTS";
            this.btnDisplayCont.UseVisualStyleBackColor = true;
            this.btnDisplayCont.Click += new System.EventHandler(this.btnDisplayCont_Click);
            // 
            // btnSingleCommand
            // 
            this.btnSingleCommand.Location = new System.Drawing.Point(12, 365);
            this.btnSingleCommand.Name = "btnSingleCommand";
            this.btnSingleCommand.Size = new System.Drawing.Size(292, 48);
            this.btnSingleCommand.TabIndex = 2;
            this.btnSingleCommand.Text = "Single Command";
            this.btnSingleCommand.UseVisualStyleBackColor = true;
            this.btnSingleCommand.Click += new System.EventHandler(this.btnSingleCommand_Click);
            // 
            // txtSingleCommand
            // 
            this.txtSingleCommand.Location = new System.Drawing.Point(12, 297);
            this.txtSingleCommand.Name = "txtSingleCommand";
            this.txtSingleCommand.Size = new System.Drawing.Size(292, 20);
            this.txtSingleCommand.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 281);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Command";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 323);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Answer";
            // 
            // txtSingleAnswer
            // 
            this.txtSingleAnswer.Location = new System.Drawing.Point(12, 339);
            this.txtSingleAnswer.Name = "txtSingleAnswer";
            this.txtSingleAnswer.Size = new System.Drawing.Size(292, 20);
            this.txtSingleAnswer.TabIndex = 5;
            // 
            // txtDisplay
            // 
            this.txtDisplay.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDisplay.Location = new System.Drawing.Point(150, 174);
            this.txtDisplay.Multiline = true;
            this.txtDisplay.Name = "txtDisplay";
            this.txtDisplay.Size = new System.Drawing.Size(228, 42);
            this.txtDisplay.TabIndex = 7;
            // 
            // btnGetKeypadPress
            // 
            this.btnGetKeypadPress.Location = new System.Drawing.Point(12, 222);
            this.btnGetKeypadPress.Name = "btnGetKeypadPress";
            this.btnGetKeypadPress.Size = new System.Drawing.Size(126, 42);
            this.btnGetKeypadPress.TabIndex = 8;
            this.btnGetKeypadPress.Text = "GET KEY PRESSED ON KEYPAD";
            this.btnGetKeypadPress.UseVisualStyleBackColor = true;
            this.btnGetKeypadPress.Click += new System.EventHandler(this.btnGetKeypadPress_Click);
            // 
            // frmCTCoin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(442, 501);
            this.Controls.Add(this.btnGetKeypadPress);
            this.Controls.Add(this.txtDisplay);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtSingleAnswer);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtSingleCommand);
            this.Controls.Add(this.btnSingleCommand);
            this.Controls.Add(this.btnDisplayCont);
            this.Controls.Add(this.groupBox1);
            this.Name = "frmCTCoin";
            this.Text = "formCTCoin";
            this.Load += new System.EventHandler(this.frmCTCoin_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnDisconnect;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.Label lblConnection;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnDisplayCont;
        private System.Windows.Forms.Button btnSingleCommand;
        private System.Windows.Forms.TextBox txtSingleCommand;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtSingleAnswer;
        private System.Windows.Forms.TextBox txtDisplay;
        private System.Windows.Forms.Button btnGetKeypadPress;
    }
}