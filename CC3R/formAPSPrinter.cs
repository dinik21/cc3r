﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ARCA;
using System.Diagnostics;

namespace CC3R
{
    public partial class frmAPSPrinter : Form
    {
        public RS232 mySerial;
        public STRUCTURES myStruct;
        public FUNCTIONS myFunc;
        public STRUCTURES.SCCTalkAnswer ccTalkAnswer = new STRUCTURES.SCCTalkAnswer();
        public string command;

        public frmAPSPrinter(string comPort)
        {
            InitializeComponent();
            InitializeComunication(comPort);

        }
        void InitializeComunication(string comPort)
        {
            mySerial = new RS232();
            myFunc = new FUNCTIONS();
            mySerial.ConfigCom(comPort);
            mySerial.DataSent += new EventHandler(ComDataSent);
            mySerial.DataReceived += new EventHandler(ComDataReceived);
        }

        public bool ControlInvokeRequired(Control c, Action a)
        {
            if (c.InvokeRequired) c.Invoke(new MethodInvoker(delegate { a(); }));
            else return false;
            return true;
        }

        public void ComDataSent(object sender, EventArgs e)//
        {
            ResetTextBox();
            grpCommands.Enabled = false;
            if (ControlInvokeRequired(txtSend, () => ComDataSent(sender, null))) return;
            txtSend.Text=sender.ToString();
        }

        public void ComDataReceived(object sender, EventArgs e)
        {
            grpCommands.Enabled = true;
            if (ControlInvokeRequired(txtHex, () => ComDataReceived(sender, null))) return;
            txtHex.Text = mySerial.commandAnswer;
            txtAscii.Text = myFunc.Hex2Ascii(mySerial.commandAnswer);
        }

        void ResetTextBox()
        {
            txtAscii.Text = "";
            txtHex.Text = "";
            txtSend.Text = "";
        }
        private void btnIdentity_Click(object sender, EventArgs e)
        {
            command = Constants.printerIdendity;
            mySerial.SendCommand(command,1,250);
        }

        private void btnStatus_Click(object sender, EventArgs e)
        {
            command = Constants.printerStatus;
            mySerial.SendCommand(command, 1, 250);
        }

        private void btnCutPartial_Click(object sender, EventArgs e)
        {
            command = Constants.printerPartialCut;
            mySerial.SendCommand(command, 1, 250);
        }

        private void btnCutFull_Click(object sender, EventArgs e)
        {
            command = Constants.printerFullCut;
            mySerial.SendCommand(command, 1, 250);
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            command = Constants.printerLineFeed;
            command = myFunc.Ascii2Hex(txtToPrint.Text) + command;
            mySerial.SendCommand(command, 1, 250);
        }

        private void btnFeedPaper_Click(object sender, EventArgs e)
        {
            command = Constants.printerFeedPaper + "FF";
            mySerial.SendCommand(command, 1, 250);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmAPSPrinter_Load(object sender, EventArgs e)
        {

        }

        private void btnSelectFont_Click(object sender, EventArgs e)
        {
            command = Constants.printerSelectFont + "01";
            mySerial.SendCommand(command, 1, 250);
        }
    }
}
